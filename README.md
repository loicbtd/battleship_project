# Battleship project

## Presentation

This is a javaFX development project of a naval battle game as part of the second semester of a computer science DUT.

##  Coded informations

### Values in playerGrid

| value    | meaning                                                     |
| -------- | ----------------------------------------------------------- |
| ]-∞,-10] | struck ship cell  (**index-10** of currentPlayer's arrShip) |
| -2       | missed shot                                                 |
| -1       | unharmed sea cell                                           |
| [0, +∞[  | unharmed ship cell (index of currentPlayer's arrShip)       |

### Values in enemyGrid

| value    | meaning                                                    |
| -------- | ---------------------------------------------------------- |
| ]-∞,-10] | struck ship cell  (**index-10** of enemytPlayer's arrShip) |
| -2       | missed shot                                                |
| -1       | unharmed sea cell                                          |

### Values returned by placeShip() method

| value | meaning              |
| ----- | -------------------- |
| -5    | superimposing ship   |
| -4    | left overrun         |
| -3    | bottom overrun       |
| -2    | right overrun        |
| -1    | top overrun          |
| 0     | ship has been placed |

### Values returned by shot() method

| value   | meaning                                                      |
| ------- | ------------------------------------------------------------ |
| -6      | left overrun                                                 |
| -5      | bottom overrun                                               |
| -4      | right overrun                                                |
| -3      | top overrun                                                  |
| -2      | cell has already been shot                                   |
| -1      | missed !                                                     |
| [0, +∞[ | struck !<br />The value is equals to the boat index of the enemyPlayer's ship array. |

### Ship sprites name format

| ship_x_y_z | Meaning                                     |
| ---------- | ------------------------------------------- |
| x          | Orientation (0 = Vertical / 1 = Horizontal) |
| y          | ID of the Ship                              |
| z          | Cell (Number of the ship's sprite)          |

### Network information encoding

#### Protocol

* Connection setup
* 









## Join the development team

### Installation of java 8 and javaFX tools

Requirements for javaFX :

#### 1. Installation of JRE 8 (Java Runtime Environment)

- Installation from the apt source from linux:

```shell
sudo apt install openjdk-8-jre
```

- Configure JRE 8 as default:

```shell
sudo update-alternatives --config java
```

> Select JRE 8 by entering the corresponding number

#### 2. Installation of the JDK 8 (Java Development Kit)

- Installation from the apt source from linux:

```shell
sudo apt install openjdk-8-jdk
```

- Configure JDK 8 as default:

```shell
sudo update-alternatives --config javac
```

> Select JDK 8 by entering the corresponding number.

#### 3.  SceneBuilder installation

- Download and install the latest version of SceneBuilder from the link below:

https://gluonhq.com/products/scene-builder/#download

- Specify the installation path of SceneBuilder from idea

> Settings > Languages & Frameworks > JavaFX



### Using the git versioning tool

> IMPORTANT: Only sync your project sources ! (especially not the /.idea and .iml)

#### Initialization

- Initialization of the current directory as a Git repository:

```shell
git init
```

- Make the link with the remote repository:

```shell
git remote add origin https://mon.url.git
```

#### Use

##### Sélection des fichiers à versionner

- Add files to the versioning process

```shell
git add myfile.extension
```

- Adding all project files to the versioning:

```shell
git add -a
```

##### Adding a commit

```shell
git commit -m "description de la version"
```

##### Upload your version to the server (remote)

```shell
git push origin master
```

##### Download the server (remote) version to your computer

```shell
git pull origin master
```

