package app.controller.fxmlController;

import app.MainApp;
import app.controller.InitPlayerGridController;
import app.model.game.LocalGame;
import app.model.player.*;
import app.model.ship.Ship;
import app.model.ship.ShipEnum;
import app.view.fxmlView.FxmlViewManager;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.AudioClip;

public class InitSoloGameController extends FxmlController{

    private int levelValue=1;

    @FXML
    private Slider level;

    @FXML
    private AnchorPane rightArrow;

    @FXML
    private AnchorPane leftArrow;

    @FXML private TextField player1Name;

    @FXML
    private ImageView levelImg;

    private Image easyImg = new Image(MainApp.class.getResource("/app/resource/image/graphic-element/level-img-easy.png").toExternalForm());;
    private Image mediumImg = new Image(MainApp.class.getResource("/app/resource/image/graphic-element/level-img-medium.png").toExternalForm());;
    private Image hardImg = new Image(MainApp.class.getResource("/app/resource/image/graphic-element/level-img-hard.png").toExternalForm());;

    @FXML
    private Label levelName;

    @FXML
    private void handleStart() {
        masterController.getAudioClip().stop();
        masterController.playMenuTheme();
        switch(levelValue){
            case 1:
                masterController.getModel().init(new LocalGame(new HumanPlayer("player1" ), new IaPlayer(new IaEasy())));
                break;
            case 2:
                masterController.getModel().init(new LocalGame(new HumanPlayer("player1" ), new IaPlayer(new IaMedium())));
                break;
            case 3:
                masterController.getModel().init(new LocalGame(new HumanPlayer("player1" ), new IaPlayer(new IaHard())));
                break;
        }
        if (player1Name.getText().length() > 0) ((HumanPlayer)masterController.getModel().getGame().getPlayer1()).setName(player1Name.getText());
        masterController.getModel().getGame().getPlayer1().init(
                10,
                        10,
                        10,
                        10,
                        new Ship[]{
        new Ship(ShipEnum.destroyer),
                new Ship(ShipEnum.cruiser),
                new Ship(ShipEnum.submarine),
                new Ship(ShipEnum.battleship),
                new Ship(ShipEnum.carrier)
    }
        );

        masterController.getModel().getGame().getPlayer2().init(
                10,
                        10,
                        10,
                        10,
                        new Ship[]{
        new Ship(ShipEnum.destroyer),
                new Ship(ShipEnum.cruiser),
                new Ship(ShipEnum.submarine),
                new Ship(ShipEnum.battleship),
                new Ship(ShipEnum.carrier)
    }
        );

        new InitPlayerGridController(masterController);


}



    @FXML
    private void handleNext() {
        clickSound();
        switch (levelValue){
            case 1:
                levelValue=2;
                levelImg.setImage(mediumImg);
                levelName.setText("Pirate");
                leftArrow.setVisible(true);

                break;
            case 2:
                levelValue=3;
                levelImg.setImage(hardImg);
                levelName.setText("Terreur");
                rightArrow.setVisible(false);
                break;
            case 3:
                break;
        }
        System.out.println(levelValue);
    }

    @FXML
    private void handlePrevious() {
        clickSound();
        switch (levelValue){
            case 1:
                break;
            case 2:
                levelValue=1;
                levelImg.setImage(easyImg);
                levelName.setText("Moussaillon");
                leftArrow.setVisible(false);
                break;
            case 3:
                levelValue=2;
                levelImg.setImage(mediumImg);
                levelName.setText("Pirate");
                rightArrow.setVisible(true);
                break;
        }
        System.out.println(levelValue);
    }
    @FXML
    private void handleBack() {
        FxmlViewManager.loadInStage("MainMenu", masterController);
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }
}
