package app.controller.fxmlController;

import app.MainApp;
import app.model.game.ClientGame;
import app.model.game.ServerGame;
import app.model.player.HumanPlayer;
import app.model.ship.Ship;
import app.model.ship.ShipEnum;
import app.view.fxmlView.FxmlViewManager;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.media.AudioClip;

public class MenuNetworkController extends FxmlController{

    @FXML private TextField playerName;

    @FXML
    private void handlePlayServer() {
        String name;
        if (playerName.getCharacters().length() != 0) {
            name = playerName.getCharacters().toString();
        }
        else {
            name = "Hôte";
        }
        masterController.getModel().setGame(new ServerGame(new HumanPlayer(name)));
        ((ServerGame)masterController.getModel().getGame()).getServer().start(0);
        masterController.getModel().getGame().getPlayer1().init(
                10,
                10,
                10,
                10,
                new Ship[]{
                        new Ship(ShipEnum.destroyer),
                        new Ship(ShipEnum.cruiser),
                        new Ship(ShipEnum.submarine),
                        new Ship(ShipEnum.battleship),
                        new Ship(ShipEnum.carrier)
                }
        );
        FxmlViewManager.loadInStage("ServerInit", masterController);
    }

    @FXML
    private void handlePlayClient() {
        String name;
        if (playerName.getCharacters().length() != 0) {
            name = playerName.getCharacters().toString();
        }
        else {
            name = "Client";
        }
        masterController.getModel().setGame(new ClientGame(new HumanPlayer(name)));
        masterController.getModel().getGame().getPlayer1().init(
                10,
                10,
                10,
                10,
                new Ship[]{
                        new Ship(ShipEnum.destroyer),
                        new Ship(ShipEnum.cruiser),
                        new Ship(ShipEnum.submarine),
                        new Ship(ShipEnum.battleship),
                        new Ship(ShipEnum.carrier)
                }
        );
        FxmlViewManager.loadInStage("ClientInit", masterController);
    }

    @FXML
    private void handleBack() {
        FxmlViewManager.loadInStage("MainMenu", masterController);
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }
}
