package app.controller.fxmlController;

import app.MainApp;
import app.controller.InitPlayerGridController;
import app.model.game.ClientGame;
import app.view.fxmlView.FxmlViewManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.media.AudioClip;


public class ClientInitController extends FxmlController {

    @FXML private TextField textfieldIp;
    @FXML private TextField textfieldPort;
    @FXML private Label labelState;

    public void afterInit() {
        labelState.setText("Saisissez l'IP et le port de l'hôte !");
    }

    @FXML
    private void handlePlay() {

        String ip = textfieldIp.getText();
        int port = Integer.parseInt(textfieldPort.getText());
        ((ClientGame)(masterController.getModel().getGame())).getClient().connectToServer(ip, port);

        new Thread(() -> {
            while(true) {
                Platform.runLater(() -> labelState.setText("Connexion à l'adversaire"));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> labelState.setText("Connexion à l'adversaire."));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> labelState.setText("Connexion à l'adversaire.."));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> labelState.setText("Connexion à l'adversaire..."));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (((ClientGame)(masterController.getModel().getGame())).getClient().isConnected()) {
                    Platform.runLater(() -> labelState.setText("Adversaire trouvé !"));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Platform.runLater(() ->  new InitPlayerGridController(masterController));
                    break;
                }
            }
        }).start();
    }

    @FXML
    private void handleBack() {
        FxmlViewManager.loadInStage("MenuNetwork", masterController);
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }
}
