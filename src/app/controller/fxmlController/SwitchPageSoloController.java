package app.controller.fxmlController;

import app.MainApp;
import app.controller.GameDemoController;
import app.controller.GameSoloController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.AudioClip;

public class SwitchPageSoloController extends  FxmlController {

    @FXML
    private Button switchPage = new Button();

    @FXML
    private Label label = new Label();

    public SwitchPageSoloController() {
    }

    @FXML
    public void button(){
        new GameSoloController(masterController);
    }

    @FXML
    public void pass(KeyEvent keyEvent){
        if (keyEvent.getCode() == KeyCode.ENTER) {
            new GameSoloController(masterController);
        }
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.50);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.40);
        masterController.getSoundEffect().play();
    }
}
