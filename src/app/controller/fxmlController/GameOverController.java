package app.controller.fxmlController;

import app.controller.GameDemoController;
import app.model.player.HumanPlayer;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class GameOverController extends FxmlController {

    @FXML
    private Button switchPage = new Button();

    @FXML
    private Label label = new Label(masterController.getModel().getGame().getWinnerPlayer());

    public GameOverController() {
    }

    @FXML
    public void button(){
        new GameDemoController(masterController);
    }

    @FXML
    public void pass(KeyEvent keyEvent){
        if (keyEvent.getCode() == KeyCode.ENTER) {
            new GameDemoController(masterController);
        }

    }
}
