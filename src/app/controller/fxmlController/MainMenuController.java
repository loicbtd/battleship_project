package app.controller.fxmlController;

import app.MainApp;
import app.view.fxmlView.FxmlViewManager;
import javafx.fxml.FXML;
import javafx.scene.media.AudioClip;

public class MainMenuController extends FxmlController {

    /**
     * empty constructor
     */
    public MainMenuController() {
    }


    /**
     * TODO: javadoc
     */
    @FXML
    private void handleMenuIa() {
        FxmlViewManager.loadInStage("InitGameSolo", masterController);
    }

    @FXML
    private void handleMenuNetwork() {
        FxmlViewManager.loadInStage("MenuNetwork", masterController);
    }


    /**
     * TODO: javadoc
     */
    @FXML
    private void handleDemoMenu() {
        FxmlViewManager.loadInStage("DemoMenu", masterController);
    }


    /**
     * TODO: javadoc
     */
    @FXML
    private void howToPlay() {FxmlViewManager.loadInStage("HowToPlay", masterController);


    }
    /**
     * TODO: javadoc
     */
    @FXML
    private void handleBack() {
        FxmlViewManager.loadInStage("Welcome", masterController);
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.50);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.40);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void cursorClick(){
        masterController.setCursorClick();
    }

    @FXML
    private void cursorBase(){
        masterController.setCursor();
    }
}