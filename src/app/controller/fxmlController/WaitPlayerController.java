package app.controller.fxmlController;

import app.MainApp;
import app.controller.GameNetworkController;
import app.model.game.ClientGame;
import app.model.game.ServerGame;
import app.view.fxmlView.FxmlViewManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.media.AudioClip;


public class WaitPlayerController extends FxmlController {

    @FXML private Label labelState;

    public void afterInit() {
        new Thread(() -> {
            while(true) {
                Platform.runLater(() -> labelState.setText("Attente de l'adversaire"));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> labelState.setText("Attente de l'adversaire."));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> labelState.setText("Attente de l'adversaire.."));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> labelState.setText("Attente de l'adversaire..."));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (masterController.getModel().getGame() instanceof ServerGame) {
                    if (((ServerGame)masterController.getModel().getGame()).getServer().readFromClient().equals("READY")) {
                        ((ServerGame)masterController.getModel().getGame()).getServer().removeInputMessage();
                        Platform.runLater(() -> FxmlViewManager.loadInStage("WaitPlayer", masterController));
                        break;
                    }

                    if (((ServerGame)masterController.getModel().getGame()).getServer().readFromClient().equals("SHOT")) {
                        ((ServerGame)masterController.getModel().getGame()).getServer().removeInputMessage();
                        ((ServerGame)masterController.getModel().getGame()).takeShotFromNetwork();
                        int status = -8;
                        while(true) {
                            status = ((ServerGame) masterController.getModel().getGame()).getStatus();
                            if(status != -8) {
                                ((ServerGame) masterController.getModel().getGame()).setStatus(-8);
                                break;
                            }
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if(status > -1 ) {
                            if(masterController.getModel().getGame().getPlayer1().hasLost()) {
                                Platform.runLater(() -> FxmlViewManager.loadInStage("EndGameDefeat", masterController));
                            }
                            else {
                                Platform.runLater(() -> FxmlViewManager.loadInStage("WaitPlayer", masterController));
                            }
                        }
                        else {
                            Platform.runLater(() -> new GameNetworkController(masterController));
                        }
                    }
                }

                if (masterController.getModel().getGame() instanceof ClientGame) {
                    if (((ClientGame)masterController.getModel().getGame()).getClient().readFromServer().equals("READY")) {
                        ((ClientGame)masterController.getModel().getGame()).getClient().removeInputMessage();
                        Platform.runLater(() -> new GameNetworkController(masterController));
                        break;
                    }

                    if (((ClientGame)masterController.getModel().getGame()).getClient().readFromServer().equals("SHOT")) {
                        ((ClientGame)masterController.getModel().getGame()).getClient().removeInputMessage();
                        ((ClientGame)masterController.getModel().getGame()).takeShotFromNetwork();

                        int status = -8;
                        while(true) {
                            status = ((ClientGame) masterController.getModel().getGame()).getStatus();
                            if(status != -8) {
                                ((ClientGame) masterController.getModel().getGame()).setStatus(-8);
                                break;
                            }
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if(status > -1 ) {
                            if(masterController.getModel().getGame().getPlayer1().hasLost()) {
                                Platform.runLater(() -> FxmlViewManager.loadInStage("EndGameDefeat", masterController));
                            }
                            else {
                                Platform.runLater(() -> FxmlViewManager.loadInStage("WaitPlayer", masterController));
                            }
                        }
                        else {
                            Platform.runLater(() -> new GameNetworkController(masterController));
                        }
                    }
                }
            }
        }).start();
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }
}
