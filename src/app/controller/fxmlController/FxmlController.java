package app.controller.fxmlController;

import app.controller.MasterController;
import javafx.fxml.FXML;

public class FxmlController {

    protected MasterController masterController;

    /**
     * empty constructor
     */
    public FxmlController() {
    }

    /**
     * masterController setter
     * @param masterController masterController
     */
    public void setMasterController(MasterController masterController) {
        this.masterController = masterController;
    }

    /**
     * initialize method
     */
    @FXML
    public void initialize() {
    }

    public void afterInit() {
        masterController.setCursor();
    }
}
