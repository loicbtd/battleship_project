package app.controller.fxmlController;

import app.view.fxmlView.FxmlViewManager;
import javafx.fxml.FXML;

public class EndGameController extends FxmlController {

    @FXML public void handleMenu() {
        masterController.getAudioClip().stop();
        masterController.playMainTheme();
        FxmlViewManager.loadInStage("MainMenu", masterController);
    }
}
