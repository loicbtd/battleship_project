package app.controller.fxmlController;

import app.MainApp;
import app.view.fxmlView.FxmlViewManager;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.AudioClip;

import javax.xml.soap.Text;

public class HowToPlayController extends FxmlController {

    boolean isCredits = false;

    @FXML
    private Label title;

    @FXML
    private Label content;

      @FXML
    private AnchorPane rightArrow;

    @FXML
    private AnchorPane leftArrow;

    @FXML
    private Button Credits;

    private String titre1 = "Lancer une partie";
    private String titre2= "Placer vos bateaux";
    private String titre3= "Déroulement de la partie";

    private String content1 = "- Contre un autre joueur:\n" +
            "\n" +
            "Dans le menu principal cliquez sur \"Mode Démonstration\" puis \"joueur contre joueur\". Ensuite, rien de plus simple, il suffit d'entrer vos noms dans les champs dédiés et d'appuyer sur \"commencer\" pour que la partie se lance. Vous pouvez laisser les champs vide et vous serez appelés player1 et player2 par défaut.\n" +
            "\n" +
            "- Contre l'ordinateur:\n" +
            "\n" +
            "Sélectionnez le mode \"Solo\" dans le menu principal. Entrez votre nom puis, à l'aide des flèches de chaque côté de l'image, sélectionnez un niveau de difficulté: Moussaillon pour facile, Pirate pour médium et Terreur pour difficile. \n" +
            "\n" +
            "- En réseau:\n" +
            "\n" +
            "Dans le menu principal, appuyez sur \"Multijoueur réseau\" et suivez les instructions.\n" +
            "\n";
    private String content2 = "La mise en place de votre grille se fait via un système de glisser-déposer. Placez votre curseur sur le bateau désiré, puis cliquez dessus et restez appuyé. Il suffit ensuite de le faire glisser jusqu'à la grille à l'emplacement souhaité. Une fois que vous êtes satisfait(e) de la disposition du bateau, vous pouvez relâcher le clic. Pour changer l'orientation du bateau, appuyez sur le bouton qui se trouve en dessous des bateaux avant de placer le navire.\n" +
            "\n" +
            "Remarque: la partie ne peut pas commencer tant que vous n'avez pas placé vos cinq bateaux.";
    private String content3 = "Chaque joueur dispose de la grille avec ses bateaux (à droite) et la grille avec ses tirs (à gauche). Il n'a pas connaissance du placement des bateaux de son adversaire et doit donc découvrir où ils se trouvent.\n" +
            "\n" +
            "Le but du jeu est de couler tous les bateaux de l'autre joueur. La partie se termine lorsqu'un joueur n'a plus aucun bateau encore à flot. \n" +
            "\n" +
            "Pour tirer il suffit de cliquer sur la case voulue. La case se colore en rouge si le tir est réussi, ou en gris s'il tombe à l'eau.\n" +
            "\n" +
            "Tant que les tirs touchent des bateaux, le joueur a le droit de rejouer. \n" +
            "\n";

    private int nOslide=1;

    public HowToPlayController() {


    }

    @FXML
    private void handleNext() {
        clickSound();
        switch (nOslide){
            case 1:
                nOslide=2;
                title.setText(titre2);
                content.setText(content2);
                leftArrow.setVisible(true);
                break;
            case 2:
                nOslide=3;
                title.setText(titre3);
                content.setText(content3);
                rightArrow.setVisible(false);
                break;
            case 3:
                break;
        }
        System.out.println(nOslide);
    }

    @FXML
    private void handlePrevious() {
        clickSound();
        switch (nOslide){
            case 1:
                break;
            case 2:
                nOslide=1;
                title.setText(titre1);
                content.setText(content1);
                leftArrow.setVisible(false);
                break;
            case 3:
                nOslide=2;
                title.setText(titre2);
                content.setText(content2);
                rightArrow.setVisible(true);
                break;
        }
        System.out.println(nOslide);

    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void handleBack() {
        if (!isCredits) FxmlViewManager.loadInStage("MainMenu", masterController);
        else {
            isCredits=false;
            Credits.setVisible(true);
            FxmlViewManager.loadInStage("HowToPlay", masterController);
        }
    }

    @FXML
    private void handleCredits() {
        isCredits=true;
        title.setText("Crédits");
        content.setAlignment(Pos.CENTER);
        content.setText("Chef de projet: Loïc Bertrand \n \n Team IA: Tom Dominguez & Quentin Delignou \n \n Team Graphismes: Tom Jacob & Mélanie Lamy");
        rightArrow.setVisible(false);
        leftArrow.setVisible(false);
        Credits.setVisible(false);
    }

}
