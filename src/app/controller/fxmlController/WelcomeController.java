package app.controller.fxmlController;

import app.MainApp;
import app.view.fxmlView.FxmlViewManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.media.AudioClip;

public class WelcomeController extends FxmlController {

    /**
     * empty constructor
     */
    public WelcomeController() {
    }

    /**
     * TODO: javadoc
     */
    @FXML
    private void handleMainMenu() {
        for (double i = 0; i < 0.20; i = i + 0.01 ){
            double volume = 0.20 - i;
            masterController.getAudioClip().setVolume(volume);
        }
        FxmlViewManager.loadInStage("MainMenu", masterController);
    }

    /**
     * TODO: javadoc
     */
    @FXML
    private void handleExit() {
        Platform.exit();
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.50);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.40);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void cursorClick() {
        masterController.setCursorClick();
    }

    @FXML
    private void cursorBase() {
        masterController.setCursor();
    }
}