package app.controller.fxmlController;

import app.MainApp;
import app.view.fxmlView.FxmlViewManager;
import javafx.fxml.FXML;
import javafx.scene.media.AudioClip;

public class DemoMenuController extends FxmlController {
    public DemoMenuController() {
    }

    @FXML
    private void playLocalMulti() {
        FxmlViewManager.loadInStage("InitPlayerName", masterController);
    }

    @FXML
    private void playDemoIA() {
        //FxmlViewManager.loadInStage("MenuIAvsIA", masterController);
    }

    @FXML
    private void handleBack() {
        FxmlViewManager.loadInStage("MainMenu", masterController);
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.50);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.40);
        masterController.getSoundEffect().play();
    }

}
