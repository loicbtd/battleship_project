package app.controller.fxmlController;

import app.MainApp;
import app.controller.InitPlayerGridController;
import app.model.game.ServerGame;
import app.view.fxmlView.FxmlViewManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.media.AudioClip;


public class ServerInitController extends FxmlController {

    @FXML private Label labelIp;
    @FXML private Label labelPort;
    @FXML private Label labelState;
    @FXML private Button buttonStart;

    public void afterInit() {
        buttonStart.setVisible(false);
        labelIp.setText(((ServerGame)(masterController.getModel().getGame())).getServer().getIp());
        labelPort.setText(String.valueOf(((ServerGame)(masterController.getModel().getGame())).getServer().getPort()));
        new Thread(() -> {
            while(true) {
                Platform.runLater(() -> labelState.setText("Recherche d'un adversaire"));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> labelState.setText("Recherche d'un adversaire."));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> labelState.setText("Recherche d'un adversaire.."));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Platform.runLater(() -> labelState.setText("Recherche d'un adversaire..."));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (((ServerGame)(masterController.getModel().getGame())).getServer().isClientConnected()) {
                    break;
                }
            }
            Platform.runLater(() -> labelState.setText("Adversaire trouvé !"));
            Platform.runLater(() -> buttonStart.setVisible(true));
        }).start();
    }

    @FXML
    private void handlePlay() {
        new InitPlayerGridController(masterController);
    }

    @FXML
    private void handleBack() {
        FxmlViewManager.loadInStage("MenuNetwork", masterController);
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }
}
