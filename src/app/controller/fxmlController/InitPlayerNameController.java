package app.controller.fxmlController;

import app.MainApp;
import app.controller.InitPlayerGridController;
import app.model.game.LocalGame;
import app.model.player.HumanPlayer;
import app.model.ship.Ship;
import app.model.ship.ShipEnum;
import app.view.fxmlView.FxmlViewManager;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.media.AudioClip;

public class InitPlayerNameController extends FxmlController {

    @FXML private TextField player1Name;
    @FXML private TextField player2Name;

    /**
     * empty constructor
     */
    public InitPlayerNameController() {
    }

    /**
     * fxml handleStart method
     */
    @FXML
    private void handleStart() {
        masterController.getAudioClip().stop();
        masterController.playMenuTheme();

        masterController.getModel().init(new LocalGame(new HumanPlayer("player1" ), new HumanPlayer("player2")));

        // init player1
        if (player1Name.getText().length() > 0) ((HumanPlayer)masterController.getModel().getGame().getPlayer1()).setName(player1Name.getText());
        masterController.getModel().getGame().getPlayer1().init(
                10,
                10,
                10,
                10,
                new Ship[]{
                        new Ship(ShipEnum.destroyer),
                        new Ship(ShipEnum.cruiser),
                        new Ship(ShipEnum.submarine),
                        new Ship(ShipEnum.battleship),
                        new Ship(ShipEnum.carrier)
                }
        );

        // init player2
        if (player2Name.getText().length() > 0)  ((HumanPlayer)masterController.getModel().getGame().getPlayer2()).setName(player2Name.getText());
        masterController.getModel().getGame().getPlayer2().init(
                10,
                10,
                10,
                10,
                new Ship[]{
                        new Ship(ShipEnum.destroyer),
                        new Ship(ShipEnum.cruiser),
                        new Ship(ShipEnum.submarine),
                        new Ship(ShipEnum.battleship),
                        new Ship(ShipEnum.carrier)
                }
        );

        new InitPlayerGridController(masterController);
    }


    /**
     * TODO: javadoc
     */
    @FXML
    private void handleBack() {
        FxmlViewManager.loadInStage("MainMenu", masterController);
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }
}