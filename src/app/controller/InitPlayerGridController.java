package app.controller;

import app.MainApp;
import app.model.game.ClientGame;
import app.model.game.ServerGame;
import app.model.player.HumanPlayer;
import app.model.player.IaPlayer;
import app.view.InitPlayerGrid;
import app.view.fxmlView.FxmlViewManager;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.media.AudioClip;

public class InitPlayerGridController extends Controller {

    private InitPlayerGrid view;

    public InitPlayerGridController(MasterController masterController) {
        super(masterController);
        view = new InitPlayerGrid(masterController.getModel(), this);
        if (masterController.getPrimaryStage().getScene() == null)
            masterController.getPrimaryStage().setScene(new Scene(view));
        else
            masterController.getPrimaryStage().getScene().setRoot(view);
        view.display();
        displayButtonNext();
    }

    public InitPlayerGrid getView() {
        return view;
    }

    public void setView(InitPlayerGrid view) {
        this.view = view;
    }

    @Override
    public void handle(Event event) {
        boolean found = false;

        // buttonBack event
        if (event.getSource().equals(view.getButtonBack())) {
            if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
                masterController.clickSound();
                masterController.getAudioClip().stop();
                masterController.playMainTheme();
                if (masterController.getModel().getGame().isPlayer1Turn()) {
                    if(masterController.getModel().getGame().getPlayer2() instanceof HumanPlayer)
                        FxmlViewManager.loadInStage("InitPlayerName", masterController);
                    if(masterController.getModel().getGame().getPlayer2() instanceof IaPlayer)
                        FxmlViewManager.loadInStage("InitGameSolo", masterController);
                }
                else {
                    masterController.getModel().getGame().setPlayer1Turn(true);
                    new InitPlayerGridController(masterController);
                }
            }
            if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
                masterController.hoverSound();
            }
        }

        // buttonHorizontal event
        if (event.getSource().equals(view.getButtonHorizontal())) {
            if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
                masterController.clickSound();
                if (view.isHorizontal()) {
                    view.getButtonHorizontal().setText("Vertical");
                    view.setHorizontal(false);
                } else {
                    view.getButtonHorizontal().setText("Horizontal");
                    view.setHorizontal(true);
                }
                found = true;
            }
            if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
                masterController.hoverSound();
            }
        }

        // buttonNext event
        if (event.getSource().equals(view.getButtonNext())) {

            // event click
            if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
                masterController.clickSound();

                if (masterController.getModel().getGame().getPlayer2() instanceof HumanPlayer) {
                    if (masterController.getModel().getGame().isPlayer1Turn()) {
                        masterController.getModel().getGame().setPlayer1Turn(false);
                        new InitPlayerGridController(masterController);
                    }
                    else {
                        masterController.getModel().getGame().setPlayer1Turn(true);
                        masterController.getAudioClip().stop();
                        masterController.playGameTheme();
                        masterController.welcomeVoice();
                        new GameDemoController(masterController);
                    }
                }

                if (masterController.getModel().getGame().getPlayer2() instanceof IaPlayer) {
                    masterController.getModel().getGame().getPlayer2().placeShip();
                    masterController.getModel().getGame().setPlayer1Turn(true);
                    masterController.getAudioClip().stop();
                    masterController.playGameTheme();
                    masterController.welcomeVoice();
                    new GameSoloController(masterController);
                }

                if (masterController.getModel().getGame() instanceof ServerGame) {
                    ((ServerGame)masterController.getModel().getGame()).getServer().writeToClient("READY");
                    masterController.getAudioClip().stop();
                    masterController.playGameTheme();
                    FxmlViewManager.loadInStage("WaitPlayer", masterController);
                }

                if (masterController.getModel().getGame() instanceof ClientGame) {
                    ((ClientGame)masterController.getModel().getGame()).getClient().writeToServer("READY");
                    masterController.getAudioClip().stop();
                    masterController.playGameTheme();
                    FxmlViewManager.loadInStage("WaitPlayer", masterController);
                }
            }

            // event mouse entered
            if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
                masterController.hoverSound();
            }

            found = true;
        }

        // shipStock event
        int row;
        int col;
        row = 0;
        while (!found && row < view.getShipStock().length) {
            if (event.getSource().equals(view.getShipStock()[row])) {
                dndOriginShipStockHandler(event, row);
                found = true;
            }
            row++;
        }

        // grid event
        row = 0;
        while (!found && row < view.getGrid().length) {
            col = 0;
            do {
                if (event.getSource().equals(view.getGrid()[row][col])) {
                    dndOriginGridHandler(event, row, col);
                    dndDestinationGridHandler(event, row, col);
                    found = true;
                }
                col++;
            } while (!found && col < view.getGrid()[row].length);
            row++;
        }
    }

    /**
     * handle drag on drop for grid as origin
     * @param event event
     * @param row row
     * @param col col
     */
    private void dndOriginGridHandler(Event event, int row, int col) {
        // drag detected on a grid element
        if (event.getEventType() == MouseEvent.DRAG_DETECTED) {
            int index = masterController.getModel().getGame().getCurrentPlayer().getPlayerGrid()[row][col]; // get the cell value which is the ship index in the player boat stock
            Dragboard db = view.getGrid()[row][col].startDragAndDrop(TransferMode.ANY); // start drag and drop gestureStackPane snappedShip = new StackPane();
//            if(index!=-1) view.updatePaneColor(view.getShipStock()[index],index);
            ImageView imageView = new ImageView(view.getShipStock()[index].snapshot(null, null));// create a drag and drop image view
            try {
                if (masterController.getModel().getGame().getCurrentPlayer().getArrShip()[index].isHorizontal()) { // get the current boat orientation
                    view.getButtonHorizontal().setText("Horizontal");
                    view.setHorizontal(true);
                }
                else {
                    view.getButtonHorizontal().setText("Vertical");
                    view.setHorizontal(false);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                view.update();
            }

            if (!view.isHorizontal()) imageView.setRotate(270); // if vertical make drag view vertical
            db.setDragView(imageView.snapshot(null, null), 25, 25);

            ClipboardContent content = new ClipboardContent(); // get the ship index and save it into the drag gesture
            content.putString(String.valueOf(index)); //TODO: get the Integer directly instead to convert it into string
            db.setContent(content);
            try {
                masterController.getModel().getGame().getCurrentPlayer().removeShip(index); // remove the ship which is going to be moved
            } catch (IndexOutOfBoundsException exception) {
                view.update();
            }
            view.update();
        }
    }


    /**
     *
     * @param event event
     * @param i index
     */
    private void dndOriginShipStockHandler(Event event, int i) {
        // drag detected on a grid element
        if (event.getEventType() == MouseEvent.DRAG_DETECTED) {
            Dragboard db = view.getShipStock()[i].startDragAndDrop(TransferMode.ANY); // start drag and drop gesture
            ImageView imageView = new ImageView(view.getShipStock()[i].snapshot(null, null)); // create a drag and drop image view
            if (!view.isHorizontal()) imageView.setRotate(270); // if vertical make drag view vertical
            db.setDragView(imageView.snapshot(null, null), 25, 25);

            ClipboardContent content = new ClipboardContent();  // get the ship index and save it into the drag gesture
            content.putString(String.valueOf(i)); //TODO: get the Integer directly instead to convert it into string
            db.setContent(content);
        }
    }

    private void dndDestinationGridHandler(Event event, int row, int col) {
        // accept dnd if fly over
        if (event.getEventType() == DragEvent.DRAG_OVER) {
            ((DragEvent)event).acceptTransferModes(TransferMode.MOVE);
            event.consume();
        }

        // draw a shadow when drag entered
        if (event.getEventType() == DragEvent.DRAG_ENTERED) {
            DragEvent e = (DragEvent)event;
            Dragboard db = e.getDragboard();
            int shipIndex = Integer.parseInt(db.getString()); // get the ship index from dnd
            try {
                int shipSize = masterController.getModel().getGame().getCurrentPlayer().getArrShip()[shipIndex].getShipEnum().getSize(); // get corresponding shipSize
                int pos;
                if (view.isHorizontal()) {
                    pos = 0;
                    while(col+pos < view.getGrid()[row].length && pos < shipSize) {
                        view.getGrid()[row][col+pos].getStyleClass().remove("texture-water");
                        view.getGrid()[row][col+pos].getStyleClass().add("texture-water-hover");
                        pos++;
                    }
                }
                else {
                    pos = 0;
                    while(row+pos < view.getGrid().length && pos < shipSize) {
                        view.getGrid()[row+pos][col].getStyleClass().remove("texture-water");
                        view.getGrid()[row+pos][col].getStyleClass().add("texture-water-hover");
                        pos++;
                    }
                }
            } catch (IndexOutOfBoundsException exception) {
                view.update();
            }
            event.consume();
        }

        // update grid when drag exited
        if (event.getEventType() == DragEvent.DRAG_EXITED) {
            view.update();
            event.consume();
        }

        // place ship when drag dropped
        if (event.getEventType() == DragEvent.DRAG_DROPPED) {
            DragEvent e = (DragEvent)event;
            Dragboard db = e.getDragboard();
            int shipIndex = Integer.parseInt(db.getString()); // get the ship index from dnd
            try {
                // place a boat and if its ok, set the current boat lives (= its size)
                masterController.getModel().getGame().getCurrentPlayer().placeShip(shipIndex, row,  col, view.isHorizontal());
            } catch (IndexOutOfBoundsException exception) {
                view.update();
            }
            view.update();
            displayButtonNext();
            e.setDropCompleted(true); // terminate dnd gesture
            event.consume();
        }
    }

    /**
     * display buttonNext only if all the ships are placed
     */
    public void displayButtonNext() {
        for (int state: masterController.getModel().getGame().getCurrentPlayer().getArrBoatState()) {
            if (state == 0) {
                view.getButtonNext().setVisible(false);
                return;
            }
        }
        view.getButtonNext().setVisible(true);
    }

    @FXML
    private void hoverSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }

    @FXML
    private void clickSound(){
        masterController.setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(masterController.getSoundEffect().isPlaying()){
            masterController.getSoundEffect().stop();
        }
        masterController.getSoundEffect().setVolume(0.20);
        masterController.getSoundEffect().play();
    }
}
