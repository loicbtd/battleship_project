package app.controller;

import app.MainApp;
import app.model.Model;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.ImageCursor;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.AudioClip;
import javafx.stage.Stage;

import java.util.Random;

public class MasterController implements EventHandler {

    private Model model;
    private Stage primaryStage;

    private AudioClip audioClip;
    private AudioClip soundEffect;
    private AudioClip voice;

    private String winnerPlayer;

    public MasterController() {
    }

    public MasterController(Model model, Stage primaryStage) {
        this.model = model;
        this.primaryStage = primaryStage;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public AudioClip getAudioClip() {
        return audioClip;
    }

    public void setAudioClip(AudioClip audioClip) {
        this.audioClip = audioClip;
    }

    public AudioClip getSoundEffect() {
        return soundEffect;
    }

    public void setSoundEffect(AudioClip soundEffect) {
        this.soundEffect = soundEffect;
    }

    public AudioClip getVoice() {
        return voice;
    }

    public void setVoice(AudioClip voice) {
        this.voice = voice;
    }

    public String getWinnerPlayer() {
        return winnerPlayer;
    }

    public void setWinnerPlayer(String winnerPlayer) {
        this.winnerPlayer = winnerPlayer;
    }

    public void setCursor(){
        Image image = new Image(MainApp.class.getResourceAsStream("resource/image/icon/cursor2.png"));
        primaryStage.getScene().getRoot().setCursor(new ImageCursor(image,50,50));
    }
    public void setCursorDrag(){
        Image image = new Image(MainApp.class.getResourceAsStream("resource/image/icon/cursor-drag.png"));
        primaryStage.getScene().getRoot().setCursor(new ImageCursor(image,50,50));
    }

    public void setCursorClick(){
        Image image = new Image(MainApp.class.getResourceAsStream("resource/image/icon/cursor-click.png"));
        primaryStage.getScene().getRoot().setCursor(new ImageCursor(image,50,50));
    }

    public void playMainTheme() {
        audioClip = new AudioClip(MainApp.class.getResource("resource/sound/main-theme-potentiel.mp3").toExternalForm());
        audioClip.setVolume(0.09);
        audioClip.setCycleCount(10);
        audioClip.play();
    }

    public void playMenuTheme() {
        audioClip = new AudioClip(MainApp.class.getResource("resource/sound/menu-theme.mp3").toExternalForm());
        audioClip.setVolume(0.15);
        audioClip.setCycleCount(10);
        audioClip.play();
    }

    public void playGameTheme() {
        Random ran = new Random();
        int nxt = (ran.nextInt(4)) + 1;
        setAudioClip(new AudioClip(MainApp.class.getResource("resource/sound/gamemusic" + nxt + ".mp3").toExternalForm()));
        if(getAudioClip().isPlaying()){
            getAudioClip().stop();
        }
        getAudioClip().setVolume(0.09);
        getAudioClip().setCycleCount(10);
        getAudioClip().play();
    }
    public void hoverSound(){
        setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-hover.wav").toExternalForm()));
        if(getSoundEffect().isPlaying()){
            getSoundEffect().stop();
        }
        getSoundEffect().setVolume(0.50);
        getSoundEffect().play();
    }
    public void clickSound(){
        setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/button-click.mp3").toExternalForm()));
        if(getSoundEffect().isPlaying()){
            getSoundEffect().stop();
        }
        getSoundEffect().setVolume(0.40);
        getSoundEffect().play();
    }
    public void shotFired(){
        setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/shot-fired-2.mp3").toExternalForm()));
        getSoundEffect().setVolume(0.09);
        getSoundEffect().play();
    }
    public void shotMissed(){
        setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/shot-missed.mp3").toExternalForm()));
        getSoundEffect().setVolume(0.30);
        getSoundEffect().play();
    }
    public void shotTouched(){
        setSoundEffect(new AudioClip(MainApp.class.getResource("resource/sound/shot-touched-2.mp3").toExternalForm()));
        if(getSoundEffect().isPlaying()){
            getSoundEffect().stop();
        }
        getSoundEffect().setVolume(0.15);
        getSoundEffect().play();
    }

    public void sinkVoice(){
        Random ran = new Random();
        int nxt = (ran.nextInt(4)) + 1;
        setVoice(new AudioClip(MainApp.class.getResource("resource/sound/voice/coule" + nxt + ".mp3").toExternalForm()));
        if(getVoice().isPlaying()){
            getVoice().stop();
        }
        getVoice().setVolume(0.45);
        getVoice().play();
    }

    public void welcomeVoice(){
        Random ran = new Random();
        int nxt = (ran.nextInt(3)) + 1;
        setVoice(new AudioClip(MainApp.class.getResource("resource/sound/voice/bienvenue" + nxt + ".mp3").toExternalForm()));
        if(getVoice().isPlaying()){
            getVoice().stop();
        }
        getVoice().setVolume(0.45);
        getVoice().play();
    }

    public void victoryVoice() {
        Random ran = new Random();
        int nxt = (ran.nextInt(3)) + 1;
        setVoice(new AudioClip(MainApp.class.getResource("resource/sound/voice/victoire" + nxt + ".mp3").toExternalForm()));
        if(getVoice().isPlaying()){
            getVoice().stop();
        }
        getVoice().setVolume(0.50);
        getVoice().play();
    }

    public void defeatVoice() {
        Random ran = new Random();
        int nxt = (ran.nextInt(2)) + 1;
        setVoice(new AudioClip(MainApp.class.getResource("resource/sound/voice/defaite" + nxt + ".mp3").toExternalForm()));
        if(getVoice().isPlaying()){
            getVoice().stop();
        }
        getVoice().setVolume(0.50);
        getVoice().play();
    }

    public boolean isEffectPlaying(){
        if(soundEffect.isPlaying()){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void handle(Event event) {

        if (event instanceof KeyEvent) {
            KeyEvent e = (KeyEvent) event;

            if (e.getCode().equals(KeyCode.M)) {
                if (audioClip.isPlaying()) {
                    audioClip.stop();
                }
                else {
                    audioClip.setVolume(0.12);
                    audioClip.play();
                }
            }

            if (e.getCode().equals(KeyCode.F11)) {
                if(primaryStage.isFullScreen()) {
                    primaryStage.setFullScreen(false);
                }
                else {
                    primaryStage.setFullScreen(true);
                }
            }
        }
    }
}
