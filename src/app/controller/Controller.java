package app.controller;

import javafx.event.Event;
import javafx.event.EventHandler;

public abstract class Controller implements EventHandler {

    protected MasterController masterController;

    public Controller(MasterController masterController) {
        this.masterController = masterController;
    }

    public MasterController getMasterController() {
        return masterController;
    }

    public void setMasterController(MasterController masterController) {
        this.masterController = masterController;
    }

    public abstract void handle(Event event);
}
