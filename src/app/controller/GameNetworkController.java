package app.controller;

import app.model.game.ClientGame;
import app.model.game.ServerGame;
import app.view.GameNetworkView;
import app.view.fxmlView.FxmlViewManager;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;

public class GameNetworkController extends Controller {

    private GameNetworkView view;

    public GameNetworkController(MasterController masterController) {
        super(masterController);
        view = new GameNetworkView(masterController.getModel(), this);
        if (masterController.getPrimaryStage().getScene() == null)
            masterController.getPrimaryStage().setScene(new Scene(view));
        else
            masterController.getPrimaryStage().getScene().setRoot(view);
        view.display();
    }

    @Override
    public void handle(Event event) {
        boolean found = false;

        // buttonBack event
        if (event.getSource().equals(view.getButtonExit())) {
            if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
                masterController.clickSound();
                //TODO : it would be better to have a confirmation
                FxmlViewManager.loadInStage("MainMenu", masterController);
            }
            if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
                masterController.hoverSound();
            }
        }

        int row;
        int col;
        // grid event
        row = 0;
        while (!found && row < view.getEnemyGrid().length) {
            col = 0;
            do {
                if (event.getSource().equals(view.getEnemyGrid()[row][col]) && event instanceof MouseEvent) {
                    MouseEvent e = (MouseEvent)event;
                    if (e.getEventType() == MouseEvent.MOUSE_ENTERED) {
                        view.getEnemyGrid()[row][col].getStyleClass().add("texture-water-hover");
                    }
                    if (e.getEventType() == MouseEvent.MOUSE_EXITED) {
                        view.getEnemyGrid()[row][col].getStyleClass().remove("texture-water-hover");
                    }
                    if (e.getEventType() == MouseEvent.MOUSE_CLICKED) {

                        int finalRow = row;
                        int finalCol = col;
                        new Thread(() -> {
                            if ( (masterController.getModel().getGame()) instanceof ServerGame
                                    || (masterController.getModel().getGame()) instanceof ClientGame) {

                                if ( (masterController.getModel().getGame()) instanceof ServerGame ) {
                                    ((ServerGame)(masterController.getModel().getGame())).shotNetwork(finalRow, finalCol);
                                }
                                if ( (masterController.getModel().getGame()) instanceof ClientGame ) {
                                    ((ClientGame)(masterController.getModel().getGame())).shotNetwork(finalRow, finalCol);
                                }

                                Platform.runLater(() -> view.setInfoLabelText("Patientez !"));

                                int shotStatus=-8;
                                while(true) {
                                    if ( (masterController.getModel().getGame()) instanceof ServerGame ) {
                                        shotStatus = ((ServerGame)(masterController.getModel().getGame())).getStatus();
                                        if (shotStatus != -8) {
                                            ((ServerGame)(masterController.getModel().getGame())).setStatus(-8);
                                            break;
                                        }
                                    }
                                    if ( (masterController.getModel().getGame()) instanceof ClientGame ) {
                                        shotStatus = ((ClientGame)(masterController.getModel().getGame())).getStatus();
                                        if (shotStatus != -8) {
                                            ((ClientGame)(masterController.getModel().getGame())).setStatus(-8);
                                            break;
                                        }
                                    }
                                    try {
                                        Thread.sleep(200);
                                    } catch (InterruptedException ex) {
                                        ex.printStackTrace();
                                    }
                                }

                                Platform.runLater(() -> view.getEnemyGrid()[finalRow][finalCol].getStyleClass().remove("texture-water-hover"));
                                Platform.runLater(() -> view.updateWaterCell(finalRow, finalCol));
//                                masterController.shotFired();

                                if(shotStatus == -1) {
                                    Platform.runLater(() -> view.setInfoLabelText("Tir raté !!"));
//                                    masterController.shotMissed();
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException ex) {
                                        ex.printStackTrace();
                                    }
                                    FxmlViewManager.loadInStage("WaitPlayer", masterController);
                                }

                                if(shotStatus == -5) {
//                                    masterController.victoryVoice();
                                    FxmlViewManager.loadInStage("EndGameVictory", masterController);
                                }

                                if(shotStatus >= 0) {
//                                    masterController.sinkVoice();
                                    Platform.runLater(() -> view.setInfoLabelText("Tir réussi ! Rejouez."));
                                }
                            }
                        }).start();


                    }
                    found = true;
                }
                col++;
            } while (!found && col < view.getEnemyGrid()[row].length);
            row++;
        }
    }
}
