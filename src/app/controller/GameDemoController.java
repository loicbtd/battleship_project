package app.controller;

import app.MainApp;
import app.view.GameDemoView;
import app.view.fxmlView.FxmlViewManager;
import javafx.event.Event;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;

public class GameDemoController extends Controller {

    private GameDemoView view;
    private boolean canPlay = true;

    public GameDemoController(MasterController masterController) {
        super(masterController);
        view = new GameDemoView(masterController.getModel(), this);
        if (masterController.getPrimaryStage().getScene() == null)
            masterController.getPrimaryStage().setScene(new Scene(view));
        else
            masterController.getPrimaryStage().getScene().setRoot(view);
        view.display();
    }

    @Override
    public void handle(Event event) {
        boolean found = false;

        // buttonBack event
        if (event.getSource().equals(view.getButtonExit())) {
            if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
                masterController.clickSound();
                masterController.getAudioClip().stop();
                masterController.getVoice().stop();
                masterController.getSoundEffect().stop();
                masterController.playMainTheme();
                //TODO : it would be better to have a confirmation
                FxmlViewManager.loadInStage("MainMenu", masterController);
            }
            if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
                masterController.hoverSound();
            }
        }

        int row;
        int col;
        // grid event
        row = 0;
        while (!found && row < view.getEnemyGrid().length) {
            col = 0;
            do {
                if (event.getSource().equals(view.getEnemyGrid()[row][col]) && event instanceof MouseEvent && canPlay) {
                    MouseEvent e = (MouseEvent)event;
                    if (e.getEventType() == MouseEvent.MOUSE_ENTERED) {
                       // view.getEnemyGrid()[row][col].setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
                        view.getEnemyGrid()[row][col].getStylesheets().add(MainApp.class.getResource("resource/stylesheet/main.css").toExternalForm());
                        view.getEnemyGrid()[row][col].getStyleClass().add("texture-water-hover");
                    }
                    if (e.getEventType() == MouseEvent.MOUSE_EXITED) {
                        view.getEnemyGrid()[row][col].getStyleClass().remove("texture-water-hover");
                    }
                    if (e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                        int shotStatus = masterController.getModel().getGame().getCurrentPlayer().shot(row, col, masterController.getModel().getGame().getStandByPlayer());
                        masterController.shotFired();
                        view.getEnemyGrid()[row][col].getStyleClass().remove("texture-water-hover");
                        view.updateWaterCell(row,col);
                        if(shotStatus == -1) {
                            view.setInfos("Le tir de l'adversaire est tombé à l'eau");
                            masterController.shotMissed();
                            canPlay = false;

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(1250);
                                        masterController.getModel().getGame().setPlayer1Turn(!masterController.getModel().getGame().isPlayer1Turn());
                                        FxmlViewManager.loadInStage("SwitchPlayer", masterController);
                                    } catch (InterruptedException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }).start();
                            //FxmlViewManager.loadInStage("SwitchPlayer", masterController);
                            //new GameDemoController(masterController);
                        }
                        else if(shotStatus >= 0) {
                            masterController.shotTouched();
                            if(masterController.getModel().getGame().getStandByPlayer().hasLost()) {
                                masterController.victoryVoice();
                                FxmlViewManager.loadInStage("EndGameVictory", masterController);
                            }
                            else {
                                int idShipTouched = masterController.getModel().getGame().getStandByPlayer().getPlayerGrid()[row][col];
                                switch(idShipTouched) {
                                    case -10:
                                        if (masterController.getModel().getGame().getStandByPlayer().getArrBoatState()[0] == 0) {
                                            view.setInfoLabelText("Barque ennemie coulée !.");
                                            masterController.sinkVoice();
                                        }
                                        break;
                                    case -11:
                                        if (masterController.getModel().getGame().getStandByPlayer().getArrBoatState()[1] == 0) {
                                            view.setInfoLabelText("Caravelle ennemie coulée !.");
                                            masterController.sinkVoice();
                                        }
                                        break;
                                    case -12:
                                        if (masterController.getModel().getGame().getStandByPlayer().getArrBoatState()[2] == 0) {
                                            view.setInfoLabelText("Brick ennemi coulé !.");
                                            masterController.sinkVoice();
                                        }
                                        break;
                                    case -13:
                                        if (masterController.getModel().getGame().getStandByPlayer().getArrBoatState()[3] == 0) {
                                            view.setInfoLabelText("Frégate ennemie coulée !.");
                                            masterController.sinkVoice();
                                        }
                                        break;
                                    case -14:
                                        if (masterController.getModel().getGame().getStandByPlayer().getArrBoatState()[4] == 0) {
                                            view.setInfoLabelText("Galion ennemi coulé !.");
                                            masterController.sinkVoice();
                                        }
                                        break;
                                    default:
                                        view.setInfoLabelText("Tir réussi ! Rejouez.");
                                        masterController.shotTouched();
                                        break;
                                }
                            }
                        }
                    }
                    found = true;
                }
                col++;
            } while (!found && col < view.getEnemyGrid()[row].length);
            row++;
        }
    }
}
