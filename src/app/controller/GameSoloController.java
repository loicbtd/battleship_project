package app.controller;

import app.view.GameSoloView;
import app.view.fxmlView.FxmlViewManager;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class GameSoloController extends Controller {

    private GameSoloView view;
    private boolean buttonClicked = true;
    private MouseEvent e;
    private boolean canPlay = true;

    public GameSoloController(MasterController masterController) {
        super(masterController);
        view = new GameSoloView(masterController.getModel(), this);
        if (masterController.getPrimaryStage().getScene() == null)
            masterController.getPrimaryStage().setScene(new Scene(view));
        else
            masterController.getPrimaryStage().getScene().setRoot(view);
        view.display();
    }

    @Override
    public void handle(Event event) {
        if (event instanceof KeyEvent) {
            KeyEvent e = (KeyEvent) event;

            if (e.getCode().equals(KeyCode.P)) {
                if (!buttonClicked) {
                    buttonClicked = true;
                    view.getGoNext().setVisible(false);
                    masterController.getModel().getGame().getPlayer2().play(masterController.getModel().getGame().getPlayer1());
                    view.updateIAGrid();
                    view.setInfoLabelText("L'adversaire a joué. A vous !");
                    //new GameSoloController(masterController);
                    if(masterController.getModel().getGame().getPlayer1().hasLost()) {
                        FxmlViewManager.loadInStage("EndGameDefeat", masterController);
                    }
                }
            }
        }
        boolean found = false;

        // buttonBack event
        if (event.getSource().equals(view.getButtonExit())) {
            if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
                masterController.clickSound();
                masterController.getAudioClip().stop();
                masterController.getVoice().stop();
                masterController.getSoundEffect().stop();
                masterController.playMainTheme();
                //TODO : it would be better to have a confirmation
                FxmlViewManager.loadInStage("MainMenu", masterController);
            }
            if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
                masterController.hoverSound();
            }
        }

        if (event.getSource().equals(view.getGoNext())) {
            if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
                masterController.clickSound();
                buttonClicked = true;
                view.getGoNext().setVisible(false);
                masterController.getModel().getGame().getPlayer2().play(masterController.getModel().getGame().getPlayer1());
                view.updateIAGrid();
                view.setInfoLabelText("L'adversaire a joué. A vous !");
                //new GameSoloController(masterController);
                if (masterController.getModel().getGame().getPlayer1().hasLost()) {
                    FxmlViewManager.loadInStage("EndGameDefeat", masterController);
                }
            }
            if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
                masterController.hoverSound();
            }
        }

        int row;
        int col;
        // grid event
        row = 0;
        while (!found && row < view.getEnemyGrid().length) {
            col = 0;
            do {
                if (event.getSource().equals(view.getEnemyGrid()[row][col]) && event instanceof MouseEvent && canPlay) {
                    e = (MouseEvent)event;
                    if (e.getEventType() == MouseEvent.MOUSE_ENTERED) {
                        // view.getEnemyGrid()[row][col].setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
                        //view.getEnemyGrid()[row][col].getStylesheets().add(MainApp.class.getResource("resource/stylesheet/main.css").toExternalForm());
                        view.getEnemyGrid()[row][col].getStyleClass().add("texture-water-hover");
                    }
                    if (e.getEventType() == MouseEvent.MOUSE_EXITED) {
                        view.getEnemyGrid()[row][col].getStyleClass().remove("texture-water-hover");
                    }
                    if (e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                        int shotStatus = masterController.getModel().getGame().getCurrentPlayer().shot(row, col, masterController.getModel().getGame().getStandByPlayer());
                        view.getEnemyGrid()[row][col].getStyleClass().remove("texture-water-hover");
                        view.updateWaterCell(row,col);
                        masterController.shotFired();
                        if(shotStatus == -1) {
                            view.setInfoLabelText("Tir raté !!");
                            masterController.shotMissed();
                            canPlay = false;

                            new Thread(() -> {
                                try {
                                    Thread.sleep(1250);
                                    masterController.getModel().getGame().getPlayer2().play(masterController.getModel().getGame().getPlayer1());
                                    if (masterController.getModel().getGame().getPlayer1().hasLost()) {
                                        masterController.defeatVoice();
                                        Platform.runLater(() -> FxmlViewManager.loadInStage("EndGameDefeat", masterController));

                                    }
                                    else {
                                        canPlay = true;
                                        view.setInfos("L'ordinateur a joué");
                                        Platform.runLater(() -> FxmlViewManager.loadInStage("SwitchPlayerSolo", masterController));
                                    }

                                } catch (InterruptedException e1) {
                                    e1.printStackTrace();
                                }
                            }).start();

                        }
                        else if(shotStatus >= 0) {
                            if(masterController.getModel().getGame().getStandByPlayer().hasLost()) {
                                masterController.victoryVoice();
                                FxmlViewManager.loadInStage("EndGameVictory", masterController);
                            }
                            else {
                                int idShipTouched = masterController.getModel().getGame().getPlayer2().getPlayerGrid()[row][col];
                                switch(idShipTouched) {
                                    case -10:
                                        if (masterController.getModel().getGame().getStandByPlayer().getArrBoatState()[0] == 0) {
                                            view.setInfoLabelText("Barque ennemie coulée !.");
                                            masterController.sinkVoice();
                                        }
                                        break;
                                    case -11:
                                        if (masterController.getModel().getGame().getStandByPlayer().getArrBoatState()[1] == 0) {
                                            view.setInfoLabelText("Caravelle ennemie coulée !.");
                                            masterController.sinkVoice();
                                        }
                                        break;
                                    case -12:
                                        if (masterController.getModel().getGame().getStandByPlayer().getArrBoatState()[2] == 0) {
                                            view.setInfoLabelText("Brick ennemi coulé !.");
                                            masterController.sinkVoice();
                                        }
                                        break;
                                    case -13:
                                        if (masterController.getModel().getGame().getStandByPlayer().getArrBoatState()[3] == 0) {
                                            view.setInfoLabelText("Frégate ennemie coulée !.");
                                            masterController.sinkVoice();
                                        }
                                        break;
                                    case -14:
                                        if (masterController.getModel().getGame().getStandByPlayer().getArrBoatState()[4] == 0) {
                                            view.setInfoLabelText("Galion ennemi coulé !.");
                                            masterController.sinkVoice();
                                        }
                                        break;
                                    default:
                                        view.setInfoLabelText("Tir réussi ! Rejouez.");
                                        masterController.shotTouched();
                                        break;
                                }
                                System.out.println(masterController.getModel().getGame().getPlayer2().getPlayerGrid()[row][col]);
                                masterController.shotTouched();
                            }
                        }
                    }
                    found = true;
                }
                col++;
            } while (!found && col < view.getEnemyGrid()[row].length);
            row++;
        }
    }
}
