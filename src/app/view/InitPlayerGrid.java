package app.view;

import app.MainApp;
import app.controller.InitPlayerGridController;
import app.model.Model;
import app.model.player.HumanPlayer;
import app.model.ship.ShipEnum;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class InitPlayerGrid extends BorderPane {

    private Model model;
    private InitPlayerGridController controller;

    private StackPane[] shipStock;
    private StackPane[][] grid;
    private Button buttonHorizontal;
    private boolean horizontal;
    private Button buttonBack;
    private Button buttonNext;

    public InitPlayerGrid(Model model, InitPlayerGridController controller) {
        this.model = model;
        this.controller = controller;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public InitPlayerGridController getController() {
        return controller;
    }

    public void setController(InitPlayerGridController controller) {
        this.controller = controller;
    }

    public StackPane[] getShipStock() {
        return shipStock;
    }

    public StackPane[][] getGrid() {
        return grid;
    }

    public Button getButtonHorizontal() {
        return buttonHorizontal;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public void setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
    }

    public Button getButtonBack() {
        return buttonBack;
    }

    public Button getButtonNext() {
        return buttonNext;
    }

    public void display() {
        initElement();
        createView();
        update();
    }

    private void initElement() {

        int nbShip = model.getGame().getCurrentPlayer().getArrShip().length;
        int gridWidth = model.getGame().getCurrentPlayer().getPlayerGrid().length;
        int gridHeight = model.getGame().getCurrentPlayer().getPlayerGrid()[0].length;

        // init shipStock elements
        shipStock = new StackPane[nbShip];
        for (int index = 0; index < shipStock.length; index++) {
            shipStock[index] = new StackPane();
        }

        // init grid elements
        grid = new StackPane[gridWidth][gridHeight];
        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[row].length; col++) {
                grid[row][col] = new StackPane();
            }
        }

        // init buttonHorizontal element
        buttonHorizontal = new Button("Horizontal");
        horizontal = true;
        buttonHorizontal.setOnMouseClicked(controller);
        buttonHorizontal.setOnMouseEntered(controller);
        buttonHorizontal.getStyleClass().addAll("btn-sm", "btn-darkblue");
        buttonHorizontal.setAlignment(Pos.CENTER);
        buttonHorizontal.setMinWidth(124);
        buttonHorizontal.setMinHeight(42);
        buttonHorizontal.setMaxWidth(124);
        buttonHorizontal.setMaxHeight(42);

        // init buttonNext element
        buttonNext = new Button("Suivant");
        buttonNext.setOnMouseClicked(controller);
        buttonNext.setOnMouseEntered(controller);
        buttonNext.setMinWidth(124);
        buttonNext.setMinHeight(42);
        buttonNext.setMaxWidth(124);
        buttonNext.setMaxHeight(42);

        // init buttonBack element
        buttonBack = new Button("Retour");
        buttonBack.setOnMouseClicked(controller);
        buttonBack.setOnMouseEntered(controller);
        buttonBack.setMinWidth(124);
        buttonBack.setMinHeight(42);
        buttonBack.setMaxWidth(124);
        buttonBack.setMaxHeight(42);
    }


    private void createView() {
        // create root view
        controller.getMasterController().getPrimaryStage().getScene().getStylesheets().add(MainApp.class.getResource("resource/stylesheet/main.css").toExternalForm());
        getStyleClass().add("background-2");
        AnchorPane anchorInitPlayerGrid = new AnchorPane();
        controller.getMasterController().setCursor();

        // create playerName view
        Label playerName = new Label(((HumanPlayer) model.getGame().getCurrentPlayer()).getName());
        playerName.setAlignment(Pos.BASELINE_CENTER);
        playerName.getStyleClass().add("h1-light");

        HBox flagContainer = new HBox();
        flagContainer.getStyleClass().add("flag-container");
        flagContainer.setPrefSize(100,100);

        HBox rosaceContainer = new HBox();
        rosaceContainer.getStyleClass().add("rosace-container");
        rosaceContainer.setPrefSize(100,100);

        HBox titlePane = new HBox();
        titlePane.getChildren().add(rosaceContainer);
        titlePane.getChildren().add(playerName);
        titlePane.getChildren().add(flagContainer);
        titlePane.setPrefSize(400,100);
        titlePane.setAlignment(Pos.CENTER);
        titlePane.getStyleClass().add("mainMenu-title-background");

        // create shipStock view
        VBox shipStockPane = new VBox();
        VBox shipStockContainer = new VBox();
        for (int index = 0; index < shipStock.length; index++) {

            shipStock[index].setPrefWidth(controller.getMasterController().getPrimaryStage().getWidth()*0.0325*model.getGame().getCurrentPlayer().getArrShip()[index].getShipEnum().getSize());
            shipStock[index].setPrefHeight(controller.getMasterController().getPrimaryStage().getHeight()*0.0475);

            shipStockContainer.getChildren().add(new HBox(shipStock[index]));
            VBox.setMargin(shipStockContainer.getChildren().get(index), new Insets(20));
        }
        shipStockContainer.getChildren().add(buttonHorizontal);
        VBox.setMargin(buttonHorizontal, new Insets(20));
        shipStockContainer.getStyleClass().add("ship-container");
        shipStockPane.getChildren().add(titlePane);
        shipStockPane.getChildren().add(shipStockContainer);
        shipStockContainer.setAlignment(Pos.CENTER);
        VBox.setMargin(titlePane,new Insets(0,0,20,0));
        shipStockContainer.setPrefSize(controller.getMasterController().getPrimaryStage().getWidth()*0.0325*6,controller.getMasterController().getPrimaryStage().getHeight()*0.0475*model.getGame().getCurrentPlayer().getArrShip().length*2.15);

        // create grid view
        GridPane gridPane = new GridPane();
        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[row].length; col++) {
                grid[row][col].setPrefWidth(controller.getMasterController().getPrimaryStage().getWidth()*0.0325);
                grid[row][col].setPrefHeight(controller.getMasterController().getPrimaryStage().getHeight()*0.0475);
                grid[row][col].getStylesheets().add(MainApp.class.getResource("resource/stylesheet/main.css").toExternalForm());
                gridPane.add(grid[row][col],col, row);
            }
        }

        //create view template//
        setCenter(anchorInitPlayerGrid);
        AnchorPane.setTopAnchor(shipStockPane,60.0);
        AnchorPane.setLeftAnchor(shipStockPane, 150.0);

        AnchorPane.setRightAnchor(gridPane, 150.0);
        AnchorPane.setTopAnchor(gridPane,60.0);
        anchorInitPlayerGrid.getChildren().addAll(shipStockPane,gridPane);

        // create buttonNext view
        buttonNext.getStyleClass().addAll("btn-sm", "btn-darkblue");
        buttonNext.setVisible(false);
        HBox hBox2 = new HBox(buttonNext);
        hBox2.setPadding(new Insets(0, 0, 10, 10));

        // create buttonBack view
        buttonBack.getStyleClass().addAll("btn-sm", "btn-darkblue");
        HBox hBox = new HBox(buttonBack);
        hBox.setPadding(new Insets(0, 10, 10, 0));
        setBottom(new BorderPane(null,null,hBox2,null,hBox));
    }

    public void update() {

        // update playerGrid view ship sprites
        boolean horizontal;
        int rowPos;
        int colPos;
        ShipEnum currentShip;
        int currentShipState;
        int currentShipSize;

        for (int index = 0; index < model.getGame().getCurrentPlayer().getArrShip().length; index++) {
            horizontal = model.getGame().getCurrentPlayer().getArrShip()[index].isHorizontal();
            rowPos = model.getGame().getCurrentPlayer().getArrShip()[index].getRow();
            colPos = model.getGame().getCurrentPlayer().getArrShip()[index].getCol();
            currentShip = model.getGame().getCurrentPlayer().getArrShip()[index].getShipEnum();
            currentShipState = model.getGame().getCurrentPlayer().getArrBoatState()[index];
            currentShipSize = model.getGame().getCurrentPlayer().getArrShip()[index].getShipEnum().getSize();

            for (int pos = 0; pos < currentShipSize; pos++) {
                if (currentShipState > 0) {
                    if (horizontal) {
                        updateShipCell(grid[rowPos][colPos+pos], 1, currentShip.getId(), pos);
                    }
                    else {
                        updateShipCell(grid[rowPos+pos][colPos], 0, currentShip.getId(), pos);
                    }
                }
            }
        }

        // update playerGrid view water
        for (int row = 0; row < model.getGame().getCurrentPlayer().getPlayerGrid().length; row++) {
            for (int col = 0; col < model.getGame().getCurrentPlayer().getPlayerGrid()[row].length; col++) {
                if (model.getGame().getCurrentPlayer().getPlayerGrid()[row][col] == -1) {
                    grid[row][col].getChildren().clear();
                    grid[row][col].getStyleClass().remove("texture-water-hover");
                    grid[row][col].getStyleClass().add("texture-water");
                    grid[row][col].setOnDragOver(controller);
                    grid[row][col].setOnDragEntered(controller);
                    grid[row][col].setOnDragExited(controller);
                    grid[row][col].setOnDragDropped(controller);
                    grid[row][col].setOnDragDetected(null);
                    grid[row][col].setOnDragDone(null);
                }
                else {
                    grid[row][col].setOnDragOver(null);
                    grid[row][col].setOnDragEntered(null);
                    grid[row][col].setOnDragExited(null);
                    grid[row][col].setOnDragDropped(null);
                    grid[row][col].setOnDragDetected(controller);
                    grid[row][col].setOnDragDone(controller);
                }
            }
        }

        // update shipStock
        for (int index = 0; index < model.getGame().getCurrentPlayer().getArrShip().length; index++) {
            if (model.getGame().getCurrentPlayer().getArrBoatState()[index] <= 0) {
                int id = model.getGame().getCurrentPlayer().getArrShip()[index].getShipEnum().getId();
                ImageView imageView = new ImageView(new Image(MainApp.class.getResourceAsStream("resource/image/ship/ship_full_"+id+".png")));
                imageView.setFitWidth(shipStock[index].getPrefWidth());
                imageView.setFitHeight(shipStock[index].getPrefHeight());
                shipStock[index].getChildren().add(imageView);
                shipStock[index].setOnDragDetected(controller);
                shipStock[index].setOnDragDone(controller);
            }
            else {
                shipStock[index].getChildren().clear();
                shipStock[index].setBackground(null);
                shipStock[index].setOnDragDetected(null);
                shipStock[index].setOnDragDone(null);
            }
        }
    }

    private void updateShipCell(StackPane stackPane, int hori, int id, int pos) {
        ImageView imageView = new ImageView(new Image(MainApp.class.getResourceAsStream("resource/image/ship/ship_"+hori+"_"+id+"_"+pos+".png")));
        imageView.setFitWidth(stackPane.getPrefWidth());
        imageView.setFitHeight(stackPane.getPrefHeight());
        stackPane.getChildren().add(imageView);
    }
}
