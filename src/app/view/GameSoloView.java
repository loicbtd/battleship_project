package app.view;

import app.MainApp;
import app.controller.GameSoloController;
import app.model.Model;
import app.model.player.HumanPlayer;
import app.model.ship.Ship;
import app.model.ship.ShipEnum;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class GameSoloView extends BorderPane {

    private Model model;
    private GameSoloController controller;

    private Label textCurrentPlayerName;
    static String info = "";

    private StackPane[][] playerGrid;
    private StackPane[][] enemyGrid;

    private HBox flagContainer;
    private HBox rosaceContainer;
    private HBox titlePane;

    private VBox vboxPlayerShipInfo;

    private Button buttonExit;
    private Button goNext;

    public GameSoloView(Model model, GameSoloController controller) {
        this.model = model;
        this.controller = controller;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public GameSoloController getController() {
        return controller;
    }

    public void setController(GameSoloController controller) {
        this.controller = controller;
    }

    public StackPane[][] getPlayerGrid() {
        return playerGrid;
    }

    public void setPlayerGrid(StackPane[][] playerGrid) {
        this.playerGrid = playerGrid;
    }

    public StackPane[][] getEnemyGrid() {
        return enemyGrid;
    }

    public void setEnemyGrid(StackPane[][] enemyGrid) {
        this.enemyGrid = enemyGrid;
    }

    public Button getButtonExit() {
        return buttonExit;
    }

    public void setButtonExit(Button buttonExit) {
        this.buttonExit = buttonExit;
    }

    public Button getGoNext() {
        return goNext;
    }

    public void setGoNext(Button goNext) {
        this.goNext = goNext;
    }

    public void display() {
        initElement();
        createView();
        update();
    }

    private void initElement() {

        // init playerGrid elements
        int playerGridNbRow = model.getGame().getCurrentPlayer().getPlayerGrid().length;
        int playerGridNbCol = model.getGame().getCurrentPlayer().getPlayerGrid()[0].length;
        playerGrid = new StackPane[playerGridNbRow][playerGridNbCol];
        for (int row = 0; row < playerGrid.length; row++) {
            for (int col = 0; col < playerGrid[row].length; col++) {
                playerGrid[row][col] = new StackPane();
            }
        }

        // init enemyGrid elements
        int enemyGridNbRow = model.getGame().getCurrentPlayer().getEnemyGrid().length;
        int enemyGridNbCol = model.getGame().getCurrentPlayer().getEnemyGrid()[0].length;
        enemyGrid = new StackPane[enemyGridNbRow][enemyGridNbCol];
        for (int row = 0; row < enemyGrid.length; row++) {
            for (int col = 0; col < enemyGrid[row].length; col++) {
                enemyGrid[row][col] = new StackPane();
            }
        }

        // init buttonExit
        buttonExit = new Button("Quitter la partie");
        buttonExit.setOnMouseClicked(controller);
        buttonExit.setOnMouseEntered(controller);
        buttonExit.setMinWidth(200);
        buttonExit.setMinHeight(50);
        buttonExit.setMaxWidth(200);
        buttonExit.setMaxHeight(50);

        goNext = new Button("! Compris !");
        goNext.setOnMouseClicked(controller);
        goNext.setOnMouseEntered(controller);
        goNext.setMinWidth(200);
        goNext.setMinHeight(50);
        goNext.setMaxWidth(200);
        goNext.setMaxHeight(50);
        goNext.setVisible(false);

    }

    private void createView() {
        // create root view
        controller.getMasterController().getPrimaryStage().getScene().getStylesheets().add(MainApp.class.getResource("resource/stylesheet/main.css").toExternalForm());
        getStyleClass().add("background-3");
        controller.getMasterController().setCursor();

        // create playerName view
        Label currentPlayerName = new Label(((HumanPlayer)model.getGame().getCurrentPlayer()).getName());
        currentPlayerName.getStyleClass().add("h1-light");

        flagContainer = new HBox();
        flagContainer.getStyleClass().add("flag-container");
        flagContainer.setMinSize(100,100);
        flagContainer.setPrefSize(150,150);
        flagContainer.setMaxSize(150,150);

        rosaceContainer = new HBox();
        rosaceContainer.getStyleClass().add("rosace-container");
        rosaceContainer.setPrefSize(150,150);
        rosaceContainer.setMinSize(100,100);
        rosaceContainer.setMaxSize(150,150);

        titlePane= new HBox() ;
        titlePane.getChildren().add(rosaceContainer);
        titlePane.getChildren().add(currentPlayerName);
        titlePane.getChildren().add(flagContainer);
        titlePane.setSpacing(25);
        titlePane.setPrefSize(400,200);
        //titlePane.setBorder(new Border(new BorderStroke(Color.BLACK,BorderStrokeStyle.SOLID,null, new BorderWidths(1))));
        titlePane.setAlignment(Pos.CENTER);
        titlePane.getStyleClass().add("mainMenu-title-background");

        //Text grilleFrappe = new Text("Grille de frappe");
        //grilleFrappe.getStyleClass().add("h1-light");


        // create playerGrid view
        GridPane playerGridPane = new GridPane();
        for (int row = 0; row < playerGrid.length; row++) {
            for (int col = 0; col < playerGrid[row].length; col++) {
                playerGrid[row][col].setPrefSize(controller.getMasterController().getPrimaryStage().getWidth()*0.025,controller.getMasterController().getPrimaryStage().getHeight()*0.040);
                playerGrid[row][col].getStylesheets().add(MainApp.class.getResource("resource/stylesheet/main.css").toExternalForm());
                playerGridPane.add(playerGrid[row][col],col, row);
            }
        }
        setRight(playerGridPane);

        // create playerGrid view
        GridPane enemyGridPane = new GridPane();
        for (int row = 0; row < enemyGrid.length; row++) {
            for (int col = 0; col < enemyGrid[row].length; col++) {
                enemyGrid[row][col].setPrefSize(controller.getMasterController().getPrimaryStage().getWidth()*0.045,controller.getMasterController().getPrimaryStage().getHeight()*0.065);
                enemyGrid[row][col].getStylesheets().add(MainApp.class.getResource("resource/stylesheet/main.css").toExternalForm());
                enemyGridPane.add(enemyGrid[row][col],col, row);
            }
        }

        AnchorPane enemyGridAnchor = new AnchorPane();
        enemyGridAnchor.setOnKeyPressed(controller);
        VBox vboxEnemyGrid = new VBox();
        VBox vboxEnemyGridV2 = new VBox();
        VBox vboxPlayerGrid = new VBox();
        vboxPlayerShipInfo = new VBox();
        //Text santeBateau = new Text("État de vos bateaux");
        Label vosBateaux = new Label("Vos Bateaux");
        HBox vosBateauxPane = new HBox(vosBateaux);

        vboxEnemyGrid.getChildren().addAll(new StackPane(titlePane),enemyGridPane);
        vboxEnemyGrid.setSpacing(40.0);

        vboxEnemyGridV2.getChildren().addAll(vboxEnemyGrid);

        createPlayerShipInfo(vboxPlayerShipInfo);


        ScrollPane sideBarScroller = new ScrollPane(vboxPlayerShipInfo);
        //sideBarScroller.setPrefHeight(230);
        sideBarScroller.setFitToWidth(true);
        sideBarScroller.setBackground(null);
        sideBarScroller.getStyleClass().add("scrollpane");
        sideBarScroller.setPrefSize(controller.getMasterController().getPrimaryStage().getWidth()*0.05,controller.getMasterController().getPrimaryStage().getHeight()*0.15);
        StackPane test = new StackPane(sideBarScroller);
        test.getStyleClass().add("mainMenu-title-background");
        StackPane.setMargin(sideBarScroller, new Insets(30,0,30,0));


        textCurrentPlayerName = new Label(((HumanPlayer)model.getGame().getCurrentPlayer()).getName());
        textCurrentPlayerName.getStyleClass().add("h2-light");
        textCurrentPlayerName.setText(info);
        HBox infobox = new HBox(textCurrentPlayerName);
        infobox.getStyleClass().add("mainMenu-title-background");
        infobox.setAlignment(Pos.CENTER);
        infobox.setPrefSize(400,controller.getMasterController().getPrimaryStage().getHeight()*0.07);

        goNext.getStyleClass().addAll("btn-sm", "btn-darkblue");

        //santeBateau.getStyleClass().add("info-bateau");
        vosBateaux.getStyleClass().add("h1-light");
        vosBateauxPane.getStyleClass().add("mainMenu-title-background");

        vosBateauxPane.setAlignment(Pos.CENTER);
        vosBateauxPane.setPrefSize(400,100);

        vboxPlayerGrid.getChildren().addAll(new StackPane(vosBateauxPane), playerGridPane, test, infobox, new StackPane(goNext));
        vboxPlayerGrid.setSpacing(20.0);
        vboxEnemyGrid.setPrefSize(controller.getMasterController().getPrimaryStage().getWidth()*0.45,controller.getMasterController().getPrimaryStage().getHeight()*0.80);


        setCenter(enemyGridAnchor);
        AnchorPane.setLeftAnchor(vboxEnemyGridV2, controller.getMasterController().getPrimaryStage().getWidth()*0.10);
        AnchorPane.setTopAnchor(vboxEnemyGridV2, controller.getMasterController().getPrimaryStage().getHeight()*0.065);

        AnchorPane.setRightAnchor(vboxPlayerGrid, controller.getMasterController().getPrimaryStage().getWidth()*0.10);
        AnchorPane.setTopAnchor(vboxPlayerGrid, controller.getMasterController().getPrimaryStage().getHeight()*0.065);

        enemyGridAnchor.getChildren().addAll(vboxEnemyGridV2, vboxPlayerGrid);
        enemyGridAnchor.setPrefSize(controller.getMasterController().getPrimaryStage().getWidth(),controller.getMasterController().getPrimaryStage().getHeight());

        // create buttonExit view
        buttonExit.getStyleClass().addAll("btn-sm", "btn-darkblue");
        AnchorPane.setBottomAnchor(buttonExit, 10.);
        AnchorPane.setLeftAnchor(buttonExit, 5.);
        enemyGridAnchor.getChildren().add(buttonExit);
    }

    public void updateWaterCell(int row, int col){
        int id = model.getGame().getCurrentPlayer().getEnemyGrid()[row][col];
        if(id == -1) {
            enemyGrid[row][col].getStyleClass().add("texture-water");
            enemyGrid[row][col].setOnMouseEntered(controller);
            enemyGrid[row][col].setOnMouseClicked(controller);
            enemyGrid[row][col].setOnMouseExited(controller);
        }
        else {
            enemyGrid[row][col].setOnMouseEntered(null);
            enemyGrid[row][col].setOnMouseClicked(null);
            enemyGrid[row][col].setOnMouseExited(null);
        }

        if(id == -2) {
            enemyGrid[row][col].getStyleClass().add("texture-water-missed");
            System.out.println("touche");
        }

        if (id <= -10) {
            enemyGrid[row][col].getStyleClass().add("texture-water-blood");
        }
    }
    public void updateIAGrid(){
        int id;

        boolean horizontal;
        int rowPos;
        int colPos;
        ShipEnum currentShip;

        for (int index = 0; index < model.getGame().getCurrentPlayer().getArrShip().length; index++) {
            horizontal = model.getGame().getCurrentPlayer().getArrShip()[index].isHorizontal();
            rowPos = model.getGame().getCurrentPlayer().getArrShip()[index].getRow();
            colPos = model.getGame().getCurrentPlayer().getArrShip()[index].getCol();
            currentShip = model.getGame().getCurrentPlayer().getArrShip()[index].getShipEnum();

            for (int pos = 0; pos < currentShip.getSize(); pos++) {
                if (horizontal) {
                    updateShipCell(playerGrid[rowPos][colPos+pos], 1, currentShip.getId(), pos, model.getGame().getCurrentPlayer().getPlayerGrid()[rowPos][colPos+pos]);
                }
                else {
                    updateShipCell(playerGrid[rowPos+pos][colPos], 0, currentShip.getId(), pos, model.getGame().getCurrentPlayer().getPlayerGrid()[rowPos+pos][colPos]);
                }
            }
        }

        for (int row = 0; row < playerGrid.length; row++) {
            for (int col = 0; col < playerGrid[row].length; col++) {
                id = model.getGame().getCurrentPlayer().getPlayerGrid()[row][col];
                if(id == -1) {
                    playerGrid[row][col].getStyleClass().add("texture-water");
                }
                if(id == -2) {
                    playerGrid[row][col].getStyleClass().add("texture-water-missed");
                }
            }
        }
    }

    public void update() {
        vboxPlayerShipInfo.getChildren().clear();
        createPlayerShipInfo(vboxPlayerShipInfo);
        int id;

        // update playerGrid view ship sprites
        boolean horizontal;
        int rowPos;
        int colPos;
        ShipEnum currentShip;

        for (int index = 0; index < model.getGame().getCurrentPlayer().getArrShip().length; index++) {
            horizontal = model.getGame().getCurrentPlayer().getArrShip()[index].isHorizontal();
            rowPos = model.getGame().getCurrentPlayer().getArrShip()[index].getRow();
            colPos = model.getGame().getCurrentPlayer().getArrShip()[index].getCol();
            currentShip = model.getGame().getCurrentPlayer().getArrShip()[index].getShipEnum();

            for (int pos = 0; pos < currentShip.getSize(); pos++) {
                if (horizontal) {
                    updateShipCell(playerGrid[rowPos][colPos+pos], 1, currentShip.getId(), pos, model.getGame().getCurrentPlayer().getPlayerGrid()[rowPos][colPos+pos]);
                }
                else {
                    updateShipCell(playerGrid[rowPos+pos][colPos], 0, currentShip.getId(), pos, model.getGame().getCurrentPlayer().getPlayerGrid()[rowPos+pos][colPos]);
                }
            }
        }

        // update playerGrid view water
        for (int row = 0; row < playerGrid.length; row++) {
            for (int col = 0; col < playerGrid[row].length; col++) {
                id = model.getGame().getCurrentPlayer().getPlayerGrid()[row][col];
                if(id == -1) {
                    playerGrid[row][col].getStyleClass().add("texture-water");
                }
                if(id == -2) {
                    playerGrid[row][col].getStyleClass().add("texture-water-missed");
                }
            }
        }

        // update enemyGrid view
        for (int row = 0; row < enemyGrid.length; row++) {
            for (int col = 0; col < enemyGrid[row].length; col++) {
                id = model.getGame().getCurrentPlayer().getEnemyGrid()[row][col];
                if(id == -1) {
                    enemyGrid[row][col].getStyleClass().add("texture-water");
                    enemyGrid[row][col].setOnMouseEntered(controller);
                    enemyGrid[row][col].setOnMouseClicked(controller);
                    enemyGrid[row][col].setOnMouseExited(controller);
                }
                else {
                    enemyGrid[row][col].setOnMouseEntered(null);
                    enemyGrid[row][col].setOnMouseClicked(null);
                    enemyGrid[row][col].setOnMouseExited(null);
                }

                if(id == -2) {
                    enemyGrid[row][col].getStyleClass().add("texture-water-missed");
                    System.out.println("touche");
                }

                if (id <= -10) {
                    enemyGrid[row][col].getStyleClass().add("texture-water-blood");
                }
            }
        }
    }


    public void updateShipCell(StackPane stackPane, int hori, int id, int pos, int alive ) {
        ImageView imageView = new ImageView(new Image(MainApp.class.getResourceAsStream("resource/image/ship/ship_"+hori+"_"+id+"_"+pos+".png")));
        imageView.setStyle("-fx-opacity: 1.0;");
        imageView.setFitWidth(stackPane.getPrefWidth());
        imageView.setFitHeight(stackPane.getPrefHeight());

        StackPane pane = new StackPane(imageView);
        pane.getStyleClass().add("texture-water");

        StackPane pane2 = new StackPane(pane, imageView);

        stackPane.getChildren().add(pane2);


        if (alive <= -10) {
            ImageView struckShip = new ImageView(new Image(MainApp.class.getResourceAsStream("resource/image/ship/ship_struck.png")));
            struckShip.setFitWidth( stackPane.getPrefWidth()*0.90);
            struckShip.setFitHeight( stackPane.getPrefHeight()*0.90);
            stackPane.getChildren().add(struckShip);
        }
    }

    /**
     * Generates health information for boats into a VBox. <br>
     * Create <B>n</B> Stackpane inside a VBox (<B>n</B> being the number of the player's boats)
     * @param vbox is the Vbox intended to contain information about boats
     */
    private void createPlayerShipInfo(VBox vbox){
        int[] arrBoatState = model.getGame().getCurrentPlayer().getArrBoatState();
        Ship[] arrShip = model.getGame().getCurrentPlayer().getArrShip();

        vbox.setSpacing(25);
        vbox.setAlignment(Pos.CENTER);
        StackPane.setMargin(vbox, new Insets(50,0,25,0));

        for(int i =0; i < arrBoatState.length; i++){
            HBox heartContainer;
            HBox destroyedContainer;
            HBox shipInfoPane;

            Label info = new Label();
            shipInfoPane= new HBox() ;
            //titlePane.setPrefSize(400,100);

            shipInfoPane.setAlignment(Pos.CENTER);

            heartContainer = new HBox();
            heartContainer.getStyleClass().add("heart-container");
            heartContainer.setPrefSize(35,25);

            destroyedContainer = new HBox();
            destroyedContainer.getStyleClass().add("destroyed-container");
            destroyedContainer.setPrefSize(35,25);


            //vbox.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
            //vbox.getStyleClass().add("mainMenu-title-background");
            info.getStyleClass().add("h2-light");

            if(arrBoatState[i] == 0){
                info.setText(arrShip[i].getShipEnum().getName() + ": Détruit");
                shipInfoPane.getChildren().add(info);
                shipInfoPane.getChildren().add(destroyedContainer);
            }
            else{
                info.setText(arrShip[i].getShipEnum().getName() + ": " + arrBoatState[i] + " x");
                shipInfoPane.getChildren().add(info);
                shipInfoPane.getChildren().add(heartContainer);
            }
            vbox.getChildren().add(shipInfoPane);

        }
    }

    public void setInfoLabelText(String text){
        textCurrentPlayerName.setText(text);
    }

    public void setInfos(String text){
        info = text;
    }

}

