package app.view.fxmlView;

import app.MainApp;
import app.controller.MasterController;
import app.controller.fxmlController.FxmlController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import java.io.IOException;

public final class FxmlViewManager {

    /**
     * private constructor to prevent instantiation
     */
    private FxmlViewManager() {
        throw new UnsupportedOperationException();
    }

    /**
     * load the fxml as parent root into the primaryStage scene
     * @param fxmlFileName fxml file name located in view/fxmlView/ with the .fxml extension
     */
    public static void loadInStage(String fxmlFileName, MasterController masterController) {

        FXMLLoader fxmlLoader;
        FxmlController controller;

        fxmlLoader = new FXMLLoader(MainApp.class.getResource("view/fxmlView/"+fxmlFileName+".fxml"));
        try {
            if (masterController.getPrimaryStage().getScene() == null)
                masterController.getPrimaryStage().setScene(new Scene(fxmlLoader.load() ));
            else
                masterController.getPrimaryStage().getScene().setRoot( fxmlLoader.load() );
        } catch (IOException ignored) {
            ignored.printStackTrace();
        }
        controller = fxmlLoader.getController();
        controller.setMasterController(masterController);
        controller.afterInit();
    }

    public static void loadInStage(String fxmlFileName, MasterController masterController, String name) {

        FXMLLoader fxmlLoader;
        FxmlController controller;

        fxmlLoader = new FXMLLoader(MainApp.class.getResource("view/fxmlView/"+fxmlFileName+".fxml"));
        try {
            if (masterController.getPrimaryStage().getScene() == null)
                masterController.getPrimaryStage().setScene(new Scene(fxmlLoader.load() ));
            else
                masterController.getPrimaryStage().getScene().setRoot( fxmlLoader.load() );
        } catch (IOException ignored) {
        }
        controller = fxmlLoader.getController();
        controller.setMasterController(masterController);
        masterController.setWinnerPlayer(name);
        controller.afterInit();
    }
}
