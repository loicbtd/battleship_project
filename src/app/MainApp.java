package app;

import app.controller.InitPlayerGridController;
import app.controller.MasterController;
import app.model.Model;
import app.model.game.LocalGame;
import app.model.player.HumanPlayer;
import app.model.player.IaEasy;
import app.model.player.IaPlayer;
import app.model.ship.Ship;
import app.model.ship.ShipEnum;
import app.view.fxmlView.FxmlViewManager;
import javafx.application.Application;
import javafx.scene.ImageCursor;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.awt.*;


public class MainApp extends Application {

    @Override
    public void start(Stage primaryStage) {

        primaryStage.getIcons().add(new Image(MainApp.class.getResourceAsStream("resource/image/icon/icon_app.png")));
        primaryStage.setTitle("Assassin's Grid");


        MasterController masterController = new MasterController(new Model(), primaryStage);
        FxmlViewManager.loadInStage("Welcome", masterController);
        primaryStage.getScene().setOnKeyPressed(masterController);
        masterController.playMainTheme();

        // TODO: LE DEV START EST ICI ET IL FAUT JUSTE COMMENTER / DECOMMENTER LE DEVSTART, RIEN DE PLUS
        devStart(masterController);
//        devStartIa(masterController);


        Font.loadFont(getClass().getResourceAsStream("resource/stylesheet/font.ttf"), 20);
        Font.loadFont(getClass().getResourceAsStream("resource/stylesheet/ConvincingPirate.ttf"), 20);
        //Image image = new Image("app/resource/image/icon/cursor.png");
        //primaryStage.getScene().getRoot().setCursor(new ImageCursor(image,50,50));

        // set primaryStage size
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//        primaryStage.setMinWidth(screenSize.getWidth());
//        primaryStage.setMinHeight(screenSize.getHeight());
//        primaryStage.setWidth(screenSize.getWidth());
//        primaryStage.setHeight(screenSize.getHeight());
        primaryStage.centerOnScreen();
        primaryStage.setFullScreen(true);
        primaryStage.setAlwaysOnTop(true);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void devStartIa(MasterController masterController) {
        masterController.getModel().init(new LocalGame(new HumanPlayer("player1" ), new IaPlayer(new IaEasy())));
        masterController.getModel().getGame().getPlayer1().init(
                10,
                10,
                10,
                10,
                new Ship[]{
                        new Ship(ShipEnum.destroyer),
                        new Ship(ShipEnum.cruiser),
                        new Ship(ShipEnum.submarine),
                        new Ship(ShipEnum.battleship),
                        new Ship(ShipEnum.carrier)
                }
        );

        masterController.getModel().getGame().getPlayer2().init(
                10,
                10,
                10,
                10,
                new Ship[]{
                        new Ship(ShipEnum.destroyer),
                        new Ship(ShipEnum.cruiser),
                        new Ship(ShipEnum.submarine),
                        new Ship(ShipEnum.battleship),
                        new Ship(ShipEnum.carrier)
                }
        );

        InitPlayerGridController controller = new InitPlayerGridController(masterController);

        masterController.getModel().getGame().getPlayer1().placeShip(0,0, 0, true);
        masterController.getModel().getGame().getPlayer1().placeShip(1,1, 0, true);
        masterController.getModel().getGame().getPlayer1().placeShip(2,2, 0, true);
        masterController.getModel().getGame().getPlayer1().placeShip(3,3, 0, true);
        masterController.getModel().getGame().getPlayer1().placeShip(4,4, 0, true);

        masterController.getModel().getGame().getPlayer2().placeShip();

        controller.getView().update();
        controller.displayButtonNext();
    }

    private void devStart(MasterController masterController) {

        masterController.getModel().init(new LocalGame(new HumanPlayer("player1" ), new HumanPlayer("player2")));
        masterController.getModel().getGame().getPlayer1().init(
                10,
                10,
                10,
                10,
                new Ship[]{
                        new Ship(ShipEnum.destroyer),
                        new Ship(ShipEnum.cruiser),
                        new Ship(ShipEnum.submarine),
                        new Ship(ShipEnum.battleship),
                        new Ship(ShipEnum.carrier)
                }
        );

        masterController.getModel().getGame().getPlayer2().init(
                10,
                10,
                10,
                10,
                new Ship[]{
                        new Ship(ShipEnum.destroyer),
                        new Ship(ShipEnum.cruiser),
                        new Ship(ShipEnum.submarine),
                        new Ship(ShipEnum.battleship),
                        new Ship(ShipEnum.carrier)
                }
        );

        InitPlayerGridController controller = new InitPlayerGridController(masterController);

        masterController.getModel().getGame().getPlayer1().placeShip(0,0, 0, true);
        masterController.getModel().getGame().getPlayer1().placeShip(1,1, 0, true);
        masterController.getModel().getGame().getPlayer1().placeShip(2,2, 0, true);
        masterController.getModel().getGame().getPlayer1().placeShip(3,3, 0, true);
        masterController.getModel().getGame().getPlayer1().placeShip(4,4, 0, true);

        masterController.getModel().getGame().getPlayer1().displayGrid();

        masterController.getModel().getGame().getPlayer2().placeShip(0,0, 0, true);
        masterController.getModel().getGame().getPlayer2().placeShip(1,1, 0, true);
        masterController.getModel().getGame().getPlayer2().placeShip(2,2, 0, true);
        masterController.getModel().getGame().getPlayer2().placeShip(3,3, 0, true);
        masterController.getModel().getGame().getPlayer2().placeShip(4,4, 0, true);

        controller.getView().update();
        controller.displayButtonNext();
    }
}
