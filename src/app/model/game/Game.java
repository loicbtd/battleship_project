package app.model.game;

import app.model.player.Player;

public abstract class Game {

    protected int roundCounter;
    protected boolean player1Turn;
    protected Player player1;
    protected Player player2;
    protected String winnerPlayer;

    public Game(Player player1, Player player2) {
        initGame(player1, player2);
    }

    public Game(Player player1) {
        initGame(player1);
    }

    public int getRoundCounter() {
        return roundCounter;
    }

    public void setRoundCounter(int roundCounter) {
        this.roundCounter = roundCounter;
    }

    public boolean isPlayer1Turn() {
        return player1Turn;
    }

    public void setPlayer1Turn(boolean player1Turn) {
        this.player1Turn = player1Turn;
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Player getCurrentPlayer() {
        return player1Turn ? player1 : player2;
    }

    public Player getStandByPlayer() {
        return player1Turn ? player2 : player1;
    }

    public String getWinnerPlayer() {
        return winnerPlayer;
    }

    public void setWinnerPlayer(String winnerPlayer) {
        this.winnerPlayer = winnerPlayer;
    }

    public void initGame(Player player1, Player player2) {
        roundCounter = 0;
        player1Turn = true;
        this.player1 = player1;
        this.player2 = player2;
    }

    public void initGame(Player player1) {
        roundCounter = 0;
        player1Turn = true;
        this.player1 = player1;
    }

    public int isGameOver() {
        if(player1.hasLost()) return 1;
        if(player2.hasLost()) return 2;
        return 0;
    }
}
