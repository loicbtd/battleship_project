package app.model.game;

import app.model.player.Player;

public class LocalGame extends Game {

    public LocalGame(Player player1, Player player2) {
        super(player1, player2);
    }
}
