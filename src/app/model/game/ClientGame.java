package app.model.game;

import app.model.network.Client;
import app.model.player.HumanPlayer;

public class ClientGame extends Game {

    private Client client;
    private volatile int status;

    public ClientGame(HumanPlayer player) {
        super(player);
        client = new Client();
        status = -8;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void shotNetwork(int row, int col) {
        status = -8;
        new Thread(() -> {
            int shotStatus;
            client.writeToServer("SHOT");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                if (client.readFromServer().equals("LISTEN_ROW")) {
                    client.removeInputMessage();
                    client.writeToServer(String.valueOf(row));
                    break;
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                if (client.readFromServer().equals("LISTEN_COL")) {
                    client.removeInputMessage();
                    client.writeToServer(String.valueOf(col));
                    break;
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                if (!(client.readFromServer().equals(""))) {
                    shotStatus = Integer.parseInt(client.readFromServer());
                    client.removeInputMessage();
                    break;
                }
            }
            if (shotStatus >= 0) player1.getEnemyGrid()[row][col] = shotStatus * (-1) - 10;  // struck !
            if (shotStatus == -1 ) player1.getEnemyGrid()[row][col] = -2; // missed
            status = shotStatus;
        }).start();
    }

    public void takeShotFromNetwork() {
        new Thread(() -> {
            int returnedValue = 0;
            int row;
            int col;

            client.writeToServer("LISTEN_ROW");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                if(!(client.readFromServer().equals(""))) {
                    row = Integer.parseInt(client.readFromServer());
                    client.removeInputMessage();
                    client.writeToServer("LISTEN_COL");
                    break;
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                if(!(client.readFromServer().equals(""))) {
                    col = Integer.parseInt(client.readFromServer());
                    client.removeInputMessage();
                    break;
                }
            }

            if (row < 0) returnedValue = -3;
            if (col < 0) returnedValue =  -6;
            if (row > player1.getPlayerGrid().length) returnedValue =  -5;
            if (col > player1.getPlayerGrid().length) returnedValue =  -4;

            int cellValue = player1.getPlayerGrid()[row][col];

            // cell already shot !
            if (cellValue == -2 || cellValue <= -10) {
                returnedValue =  -2;
            }

            //struck !
            if (cellValue >= 0) {
                player1.getArrBoatState()[cellValue]--;
                returnedValue = cellValue;
                player1.getPlayerGrid()[row][col] = cellValue * (-1) - 10;
            }

            // missed !
            if (cellValue == -1 ) {
                returnedValue = cellValue;
                player1.getPlayerGrid()[row][col] = -2;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (player1.hasLost()) {
                client.writeToServer(String.valueOf(-5));
            }
            else {
                client.writeToServer(String.valueOf(returnedValue));
            }
            status = returnedValue;
        }).start();
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
