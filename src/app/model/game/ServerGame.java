package app.model.game;

import app.model.network.Server;
import app.model.player.HumanPlayer;

public class ServerGame extends Game {

    private Server server;
    private volatile int status;

    public ServerGame(HumanPlayer player) {
        super(player);
        server = new Server();
        status = -8;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void shotNetwork(int row, int col) {
        new Thread(() -> {
            status = -8;
            int shotStatus;
            server.writeToClient("SHOT");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                if (server.readFromClient().equals("LISTEN_ROW")) {
                    server.removeInputMessage();
                    server.writeToClient(String.valueOf(row));
                    break;
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                if (server.readFromClient().equals("LISTEN_COL")) {
                    server.removeInputMessage();
                    server.writeToClient(String.valueOf(col));
                    break;
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                if (!(server.readFromClient().equals(""))) {
                    shotStatus = Integer.parseInt(server.readFromClient());
                    server.removeInputMessage();
                    break;
                }
            }
            if (shotStatus >= 0) player1.getEnemyGrid()[row][col] = shotStatus * (-1) - 10;  // struck !
            if (shotStatus == -1 ) player1.getEnemyGrid()[row][col] = -2; // missed
            status = shotStatus;
        }).start();
    }

    public void takeShotFromNetwork() {
        new Thread(() -> {
            status = -8;
            int returnedValue = 0;
            int row;
            int col;

            server.writeToClient("LISTEN_ROW");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                if(!(server.readFromClient().equals(""))) {
                    row = Integer.parseInt(server.readFromClient());
                    server.removeInputMessage();
                    server.writeToClient("LISTEN_COL");
                    break;
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                if(!(server.readFromClient().equals(""))) {
                    col = Integer.parseInt(server.readFromClient());
                    server.removeInputMessage();
                    break;
                }
            }

            if (row < 0) returnedValue = -3;
            if (col < 0) returnedValue =  -6;
            if (row > player1.getPlayerGrid().length) returnedValue =  -5;
            if (col > player1.getPlayerGrid().length) returnedValue =  -4;

            int cellValue = player1.getPlayerGrid()[row][col];

            // cell already shot !
            if (cellValue == -2 || cellValue <= -10) {
                returnedValue =  -2;
            }

            //struck !
            if (cellValue >= 0) {
                player1.getArrBoatState()[cellValue]--;
                returnedValue = cellValue;
                player1.getPlayerGrid()[row][col] = cellValue * (-1) - 10;
            }

            // missed !
            if (cellValue == -1 ) {
                returnedValue = cellValue;
                player1.getPlayerGrid()[row][col] = -2;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (player1.hasLost()) {
                server.writeToClient(String.valueOf(-5));
            }
            else {
                server.writeToClient(String.valueOf(returnedValue));
            }
            status = returnedValue;
        }).start();
    }

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }
}