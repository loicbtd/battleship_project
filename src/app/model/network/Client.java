package app.model.network;

import java.io.*;
import java.net.Socket;

public class Client {

    private final static long HEARTBEAT_DELAY = 1000;
    private final static String HEARTBEAT_MSG = "HEARTBEAT";

    private Socket socket;
    private volatile boolean up;
    private volatile boolean connected;

    private String ip;
    private int port;
    private volatile String inputMessage;
    private volatile String outPutMessage;

    /**
     * empty constructor
     */
    public Client() {
        inputMessage = "";
    }

    public void stop() {
        up = false;
        try {
            socket.close();
            Thread.sleep(10);
        } catch (Exception ignored) {
        }
        connectToServer(ip,port);
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String readFromServer() {
        return inputMessage;
    }

    public void removeInputMessage() {
        inputMessage = "";
    }

    public void writeToServer(String outPutMessage) {
        this.outPutMessage = outPutMessage;
    }

    /**
     * setup the connection to the server
     * @param ip ip address of the server
     * @param port port listened by the server
     */
    public void connectToServer(String ip, int port) {

        this.ip = ip;
        this.port = port;
        up = true;
        connected = false;

        // setup the connection with the server in a thread
        Thread clientSetup = new Thread(() -> {
            while (true) {
                try {
                    socket = new Socket(ip, port);
                    if(socket.isConnected()){
                        connected = true;
                        Thread.sleep(500);
                        break;
                    }
                } catch (Exception ignored) {
                }
            }
            afterConnection();
        }, "client-setup-thread");
        clientSetup.start();


    }

    private void afterConnection() {

        // start to listen to the server in a thread
        Thread listener = new Thread(() -> {
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String string;
                inputMessage = "";
                int counter;
                while(up && connected) {
                    counter = 0;
                    do {
                        Thread.sleep(HEARTBEAT_DELAY / 10);
                        string = bufferedReader.readLine();
                        if (string != null) break;
                        counter++;
                    } while(counter < 10);

                    if(counter >= 9) {
                        connected = false;
                        bufferedReader.close();
                        socket.close();
                        while(true) {
                            if(socket.isClosed()) {
                                connectToServer(ip,port);
                                break;
                            }
                        }
                        break;
                    }

                    if (string.equals(HEARTBEAT_MSG)) continue;
                    inputMessage = string;
//                    System.out.println("inputMessage> "+inputMessage);
                }
            }
            catch (Exception ignored) {
            }
        }, "client-listener-thread");
        listener.start();

        Thread writer = new Thread(() -> {

            try {
                PrintWriter printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                while(up && connected) {
                    // if output message is not null, send it to client
                    if (outPutMessage != null) {
                        printWriter.println(outPutMessage);
//                        System.out.println("outPutMessage> "+outPutMessage);
                        outPutMessage = null;
                    }
                    else {
                        printWriter.println(HEARTBEAT_MSG);
                    }
                    Thread.sleep(500);
                }
                printWriter.close();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }, "client-writer-thread");
        writer.start();
    }
}
