package app.model.network;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private final static long HEARTBEAT_DELAY = 1000;
    private final static String HEARTBEAT_MSG = "HEARTBEAT";

    private Socket socket;
    private ServerSocket serverSocket;
    private volatile boolean up;
    private volatile boolean clientConnected;

    private String ip;
    private int port;
    private String inputMessage;
    private String outPutMessage;

    public Server() {
        inputMessage = "";
        socket = new Socket();
    }

    public void stop() {
        up = false;
        try {
            serverSocket.close();
            socket.close();
        } catch (Exception ignored) {
        }
    }

    public boolean isClientConnected() {
        return clientConnected;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String readFromClient() {
        return inputMessage;
    }

    public void removeInputMessage() {
        inputMessage = "";
    }

    public void writeToClient(String outPutMessage) {
        this.outPutMessage = outPutMessage;
    }

    public void start(int port) {

        this.port = port;

        try {
            // set ip
            Socket socketForIp = new Socket();
            socketForIp.connect(new InetSocketAddress("google.com", 80));
            ip = socketForIp.getLocalAddress().getHostAddress();

        } catch (Exception exception) {
            exception.printStackTrace();
        }

        // wait a client in a thread
        Thread serverSetup = new Thread(() -> {
            try {
                // waiting for client
                up = true;
                serverSocket = new ServerSocket(6666);
                this.port = serverSocket.getLocalPort();
                socket = serverSocket.accept();
                clientConnected = true;
                Thread.sleep(500);
                afterSetup();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "server-setup-thread");
        serverSetup.start();
    }

    private void afterSetup() {
        // start to listen to the client in a thread
        Thread listener = new Thread(() -> {

            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String string;
                int counter;
                while(up && clientConnected) {
                    counter = 0;
                    do {
                        Thread.sleep(HEARTBEAT_DELAY / 10);
                        string = bufferedReader.readLine();
                        if (string != null) break;
                        counter++;
                    } while(counter < 10);

                    if(counter >= 9) {
                        clientConnected = false;
                        Thread.sleep(5000);
                        bufferedReader.close();
                        socket.close();
                        serverSocket.close();
                        while(true) {
                            if(socket.isClosed() && serverSocket.isClosed()) {
                                start(port);
                                break;
                            }
                        }
                        break;
                    }

                    clientConnected = true;
                    if (string.equals(HEARTBEAT_MSG)) continue;
                    inputMessage = string;
//                    System.out.println("inputMessage> "+inputMessage);
                }
            }
            catch (Exception ignored) {
            }
        }, "server-listener-thread");
        listener.start();


        // start the writer
        Thread writer = new Thread(() -> {

            try {
                PrintWriter printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                while(up && clientConnected) {
                    // if output message is not null, send it to client
                    if (outPutMessage != null) {
                        printWriter.println(outPutMessage);
//                        System.out.println("outPutMessage> "+outPutMessage);
                        outPutMessage = null;
                    }
                    else {
                        printWriter.println(HEARTBEAT_MSG);
                    }
                    Thread.sleep(500);
                }
                printWriter.close();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }, "server-writer-thread");
        writer.start();
    }
}