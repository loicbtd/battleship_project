package app.model.ship;

public enum ShipEnum {
    destroyer(0, 2, "Barque"),
    cruiser(1, 3, "Caravelle"),
    submarine(2, 3, "Brick"),
    battleship(3, 4, "Frégate"),
    carrier(4, 5, "Galion");

    private int id;
    private int size;
    private String name;

    ShipEnum(int id, int size, String name) {
        this.id = id;
        this.size = size;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
