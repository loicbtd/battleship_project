package app.model.ship;

public class Ship {

    private ShipEnum shipEnum;
    private int row;
    private int col;
    private boolean horizontal;

    public Ship(ShipEnum shipEnum) {
        this.shipEnum = shipEnum;
    }

    public Ship(ShipEnum shipEnum, int row, int col, boolean horizontal) {
        this.shipEnum = shipEnum;
        this.row = row;
        this.col = col;
        this.horizontal = horizontal;
    }

    public ShipEnum getShipEnum() {
        return shipEnum;
    }

    public void setShipEnum(ShipEnum shipEnum) {
        this.shipEnum = shipEnum;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public void setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
    }
}
