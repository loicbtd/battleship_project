package app.model;

import app.model.game.Game;

public class Model {

    private Game game;

    public Model() {
    }

    public void init(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
}