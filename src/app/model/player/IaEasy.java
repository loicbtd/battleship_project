package app.model.player;

public class IaEasy implements Ia {

    public void play(Player currentPlayer, Player enemyPlayer) {
        int [] coordonates;
        do {
            coordonates = chooseTarget(currentPlayer);
        } while (currentPlayer.shot(coordonates[0],coordonates[1],enemyPlayer) < -1);
        if(currentPlayer.getEnemyGrid()[coordonates[0]][coordonates[1]] < -2){
            play(currentPlayer,enemyPlayer);
        }
    }

    public int[] chooseTarget(Player currentPlayer){
        int row = (int) (Math.random()*currentPlayer.getEnemyGrid().length);
        int col = (int) (Math.random()*currentPlayer.getEnemyGrid()[0].length);
        int coordonnées[] = {row,col};
        return coordonnées;
    }

}
