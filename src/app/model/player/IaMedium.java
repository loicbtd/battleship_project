package app.model.player;
import java.util.ArrayList;
import java.util.Arrays;

public class IaMedium implements Ia {

    private int[] previousTarget = new int[2];
    private ArrayList<int[]> touchedCells = new ArrayList<>();

    private int[] tryToSink = {0,0,0,0,0};
    private int[] shipState = {2,3,3,4,5};

    private boolean rounded = false;
    private int indice =0;


    public void play(Player currentPlayer, Player enemyPlayer) {
        int [] coordonates = {-1,0};
        while (currentPlayer.shot(coordonates[0],coordonates[1],enemyPlayer) < -1) {
            coordonates = chooseTarget(currentPlayer, enemyPlayer);
        }

        if (currentPlayer.getEnemyGrid()[coordonates[0]][coordonates[1]] <-2 )
            play(currentPlayer,enemyPlayer);
    }




    public int[] chooseTarget(Player currentPlayer, Player enemyPlayer) {
        int row = 0, col = 0;
        int[] coordonates = {row, col};
        boolean alreadySet = true, touched = false;

        // Tests pour savoir si un bateau a été touché :

        // Si il y a déjà eu un tir
        if (previousTarget[0] > -1 && previousTarget[1] > -1) {

            // On fixe l'indice de parcours des tableaux
            int index = currentPlayer.getEnemyGrid()[previousTarget[0]][previousTarget[1]];
            index = (index + 10) * -1;

            // Si on a touché
            if (currentPlayer.getEnemyGrid()[previousTarget[0]][previousTarget[1]] < -2 &&
                    shipState[index]!=0) {

                if (touchedCells.size()==0){
                    touchedCells.add(previousTarget);
                    touched = true;
                }
                else {
                    for (int i = 0; i < touchedCells.size(); i++) {
                        if (Arrays.equals(touchedCells.get(i), previousTarget))
                            alreadySet = true;
                        else
                            alreadySet = false;
                    }
                    if (!alreadySet) {
                        // On ajoute la case à la liste des cases touchées
                        touchedCells.add(previousTarget);
                        touched = true;
                    }
                }

                // On fait des tests pour savoir si un bateau a été coulé

                // Si l'index correspond à celui d'un bateau
                if (index >= 0 && index <= 4) {
                    tryToSink[index]++; // Le nombre de frappes sur le bateau augmente
                    shipState[index]--; // Le nombre de vies du bateau diminue

                    // Si le bateau n'a plus de vie
                    if (shipState[index] == 0) {
                        // On supprime les cases du bateau de la liste des cases touchées

                        if (enemyPlayer.getArrShip()[index].isHorizontal()) {
                            int[] cellToDelete = new int[2];
                            for (int i = 0; i < tryToSink[index]; i++) {
                                cellToDelete[0] = enemyPlayer.getArrShip()[index].getRow();
                                cellToDelete[1] = enemyPlayer.getArrShip()[index].getCol() + i;
                                for (int j = 0; j < touchedCells.size(); j++) {
                                    if (touchedCells.get(j)[0] == cellToDelete[0] && touchedCells.get(j)[1] == cellToDelete[1]) {
                                        touchedCells.remove(j);
                                    }
                                }
                            }
                            indice = indice - tryToSink[index];
                        } else {
                            int[] cellToDelete = new int[2];
                            for (int i = 0; i < tryToSink[index]; i++) {
                                cellToDelete[0] = enemyPlayer.getArrShip()[index].getRow() + i;
                                cellToDelete[1] = enemyPlayer.getArrShip()[index].getCol();
                                for (int j = 0; j < touchedCells.size(); j++) {
                                    if (touchedCells.get(j)[0] == cellToDelete[0] && touchedCells.get(j)[1] == cellToDelete[1]) {
                                        touchedCells.remove(j);
                                    }
                                }
                            }
                            indice = indice - tryToSink[index];
                        }
                    }

                }
                if (touchedCells.isEmpty()){
                    touched = false;
                }

            }
            else if (!touchedCells.isEmpty()) {
                touched = true;
            }
            else {
                touched = false;
            }
        }


        // Si un bateau a été touché
        if (touched) {

            coordonates = adaptativeSituation(currentPlayer, 0);

            previousTarget = coordonates;
            return  coordonates;
        }
        else {
            row = (int) (Math.random() * currentPlayer.getEnemyGrid().length);
            col = (int) (Math.random() * currentPlayer.getEnemyGrid()[0].length);
            coordonates[0]=row;
            coordonates[1]=col;
            previousTarget =  coordonates;
            return coordonates;
        }
    }

    public int[] adaptativeSituation(Player currentPlayer, int indice) {
        int[] coordonates = new int[2];
        int row, col;

        int round[]=turnAround(currentPlayer,touchedCells.get(0)[0],touchedCells.get(0)[1]);
        int checkedCells = 0;

        for (int i=0;i<round.length;i++){
            if (round[i]!=-1){
                checkedCells++;
            }
        }

        if (checkedCells==4){
            touchedCells.remove(0);
            adaptativeSituation(currentPlayer,0);
        }
        else {
            if (round[0] == -1) {
                row = touchedCells.get(indice)[0];
                col = touchedCells.get(indice)[1] + 1;
                coordonates[0] = row;
                coordonates[1] = col;
            } else if (round[1] == -1) {
                row = touchedCells.get(indice)[0] + 1;
                col = touchedCells.get(indice)[1];
                coordonates[0] = row;
                coordonates[1] = col;
            } else if (round[2] == -1) {
                row = touchedCells.get(indice)[0];
                col = touchedCells.get(indice)[1] - 1;
                coordonates[0] = row;
                coordonates[1] = col;
            } else if (round[3] == -1) {
                row = touchedCells.get(indice)[0] - 1;
                col = touchedCells.get(indice)[1];
                coordonates[0] = row;
                coordonates[1] = col;
            }
        }

        return  coordonates;

    }

    public int[] turnAround(Player currentPlayer, int row, int col){
        int[] round = new int[4];

        if (col+1 < currentPlayer.getEnemyGrid().length)
            round[0] = currentPlayer.getEnemyGrid()[row][col+1];
        else
            round[0] = 10;

        if (row+1 < currentPlayer.getEnemyGrid().length)
            round[1]= currentPlayer.getEnemyGrid()[row+1][col];
        else
            round[1] = 10;

        if (col-1 >= 0)
            round[2]= currentPlayer.getEnemyGrid()[row][col-1];
        else
            round[2] = 10;

        if (row-1 >= 0)
            round[3]= currentPlayer.getEnemyGrid()[row-1][col];
        else
            round[3] = 10;

        return round;
    }

}