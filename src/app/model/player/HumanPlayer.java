package app.model.player;

public class HumanPlayer extends Player {

    private String name;

    public HumanPlayer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int placeShip(int shipIndex, int row, int col, boolean horizontal) {
        return super.placeShip(shipIndex, row, col, horizontal);
    }

    public void placeShip() {
        throw new UnsupportedOperationException("HumanPlayer instances can't place their ships automatically");
    }

    public void removeShip(int shipIndex) {
        super.removeShip(shipIndex);
    }

    public int shot(int row, int col, Player enemyPlayer) {
        return super.shot(row, col, enemyPlayer);
    }

    public int takeShot(int row, int col) {
        return super.takeShot(row, col);
    }

    public void play(Player enemyPlayer) {
        throw new UnsupportedOperationException("play() method of HumanPlayer can't be used");
    }

}
