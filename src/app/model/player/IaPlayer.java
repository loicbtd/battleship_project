package app.model.player;


import app.model.ship.Ship;

import java.util.Random;

public class IaPlayer extends Player {

    private Ia ia;

    public IaPlayer(Ia ia) {
        this.ia = ia;
    }

    public void init(int playerGridRow, int playerGridCol, int enemyGridRow, int enemyGridCol, Ship[] arrShip) {
        super.init(playerGridRow, playerGridCol, enemyGridRow, enemyGridCol, arrShip);
    }

    public int placeShip(int shipIndex, int row, int col, boolean horizontal) {
        return super.placeShip(shipIndex, row, col, horizontal);
    }

    public void placeShip() {
        int row, col;
        boolean horizontal;
        Random random  = new Random();

        for (int shipIndex=0 ; shipIndex<this.arrShip.length ; shipIndex ++) {
            horizontal = random.nextBoolean();
            row = (int) (Math.random()*this.enemyGrid.length-1);
            col = (int) (Math.random()*this.enemyGrid.length-1);
            if(this.placeShip(shipIndex, row, col, horizontal) != 0)
                shipIndex --;
        }
    }


    public int shot(int row, int col, Player enemyPlayer) {
        return super.shot(row, col, enemyPlayer);
    }

    public int takeShot(int row, int col) {
        return super.takeShot(row, col);
    }

    public void play(Player enemyPlayer) {
        ia.play(this, enemyPlayer);
    }

}
