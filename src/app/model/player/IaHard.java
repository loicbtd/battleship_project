package app.model.player;

import java.util.ArrayList;
import java.util.Arrays;

public class IaHard implements Ia {

    private int[] previousTarget = new int[2];
    private ArrayList<int[]> touchedCells = new ArrayList<>();

    private int[] tryToSink = {0,0,0,0,0};
    private int[] shipState = {2,3,3,4,5};


    public void play(Player currentPlayer, Player enemyPlayer) {
        int [] coordonates = {-1,0};
        while (currentPlayer.shot(coordonates[0],coordonates[1],enemyPlayer) < -1) {
            coordonates = chooseTarget(currentPlayer, enemyPlayer);
        }

        if (currentPlayer.getEnemyGrid()[coordonates[0]][coordonates[1]] <-2 )
            play(currentPlayer,enemyPlayer);
    }




    public int[] chooseTarget(Player currentPlayer, Player enemyPlayer){
        int row = 0, col = 0, situation = 0;
        int[] coordonates = {row,col};
        boolean alreadySet = true, touched = false;

        // Tests pour savoir si un bateau a été touché :

        // Si il y a déjà eu un tir
        if (previousTarget[0] > -1 && previousTarget[1] > -1) {

            // On fixe l'indice de parcours des tableaux
            int index = currentPlayer.getEnemyGrid()[previousTarget[0]][previousTarget[1]];
            index = (index + 10) * -1;

            // Si on a touché
            if (currentPlayer.getEnemyGrid()[previousTarget[0]][previousTarget[1]] < -2 &&
                shipState[index]!=0) {

                if (touchedCells.size()==0){
                    touchedCells.add(previousTarget);
                    touched = true;
                }
                else {
                    for (int i = 0; i < touchedCells.size(); i++) {
                        if (Arrays.equals(touchedCells.get(i), previousTarget))
                            alreadySet = true;
                        else
                            alreadySet = false;
                    }
                    if (!alreadySet) {
                        // On ajoute la case à la liste des cases touchées
                        touchedCells.add(previousTarget);
                        touched = true;
                    }
                }

                // On fait des tests pour savoir si un bateau a été coulé

                // Si l'index correspond à celui d'un bateau
                if (index >= 0 && index <= 4) {
                    tryToSink[index]++; // Le nombre de frappes sur le bateau augmente
                    shipState[index]--; // Le nombre de vies du bateau diminue

                    // Si le bateau n'a plus de vie
                    if (shipState[index] == 0) {
                        // On supprime les cases du bateau de la liste des cases touchées

                        if (enemyPlayer.getArrShip()[index].isHorizontal()) {
                            int[] cellToDelete = new int[2];
                            for (int i = 0; i < tryToSink[index]; i++) {
                                cellToDelete[0] = enemyPlayer.getArrShip()[index].getRow();
                                cellToDelete[1] = enemyPlayer.getArrShip()[index].getCol() + i;
                                for (int j = 0; j < touchedCells.size(); j++) {
                                    if (touchedCells.get(j)[0] == cellToDelete[0] && touchedCells.get(j)[1] == cellToDelete[1]) {
                                        touchedCells.remove(j);
                                    }
                                }
                            }
                        } else {
                            int[] cellToDelete = new int[2];
                            for (int i = 0; i < tryToSink[index]; i++) {
                                cellToDelete[0] = enemyPlayer.getArrShip()[index].getRow() + i;
                                cellToDelete[1] = enemyPlayer.getArrShip()[index].getCol();
                                for (int j = 0; j < touchedCells.size(); j++) {
                                    if (touchedCells.get(j)[0] == cellToDelete[0] && touchedCells.get(j)[1] == cellToDelete[1]) {
                                        touchedCells.remove(j);
                                    }
                                }
                            }
                        }
                    }

                }
                if (touchedCells.isEmpty()){
                    touched = false;
                }

            }
            else if (!touchedCells.isEmpty()) {
                touched = true;
            }
            else {
                touched = false;
            }
        }


        // Si un bateau a été touché
        if (touched){

            // Test pour savoir si la case touchée est "seule"
            if (touchedCells.size()==1){
                situation = 0;
            }

            // Si on a touché plus d'une case :
            else{
                // Si la colonne de la dernière case touchée est supérieure à celle de la première case touchée
                if (touchedCells.get(touchedCells.size()-1)[0] == touchedCells.get(0)[0] && (touchedCells.get(touchedCells.size()-1)[1] > touchedCells.get(0)[1])){
                    if (Arrays.equals(previousTarget,touchedCells.get(touchedCells.size()-1))){
                        situation = 1;
                    }
                    else {
                        situation = 2;
                    }
                }

                // Si la colonne de la dernière case touchée est inférieure à celle de la première case touchée
                else if (touchedCells.get(touchedCells.size()-1)[0] == touchedCells.get(0)[0] && touchedCells.get(touchedCells.size()-1)[1] < touchedCells.get(0)[1]) {
                    if (Arrays.equals(previousTarget,touchedCells.get(touchedCells.size()-1))){
                        situation = 3;
                    }
                    else {
                        situation = 4;
                    }
                }

                // Si la ligne de la dernière case touchée est supérieure à celle de la première case touchée
                else if (touchedCells.get(touchedCells.size()-1)[0] > touchedCells.get(0)[0] && (touchedCells.get(touchedCells.size()-1)[1] == touchedCells.get(0)[1])){
                    if (Arrays.equals(previousTarget,touchedCells.get(touchedCells.size()-1))){
                        situation = 5;
                    }
                    else {
                        situation = 6;
                    }
                }
                // Si la ligne de la dernière case touchée est inférieure à celle de la première case touchée
                else if (touchedCells.get(touchedCells.size()-1)[0] < touchedCells.get(0)[0] && (touchedCells.get(touchedCells.size()-1)[1] == touchedCells.get(0)[1])){
                    situation = 7;
                }
            }

            coordonates = adaptativeSituation(currentPlayer,situation);
            previousTarget = coordonates;
            return  coordonates;

        }

        // Sinon tir random
        else {
            row = (int) (Math.random() * currentPlayer.getEnemyGrid().length);
            col = (int) (Math.random() * currentPlayer.getEnemyGrid()[0].length);
            coordonates[0]=row;
            coordonates[1]=col;
            previousTarget =  coordonates;
            return coordonates;
        }

    }

    public int[] adaptativeSituation(Player currentPlayer, int situation){
        int[] coordonates = new int[2];
        int row, col;

        // Selon les situations :
        switch (situation){
            // La case est la seule touchée, on essaie de tourner autour pour trouver l'axe du bateau
            case 0:
                int round[] = turnAround(currentPlayer,touchedCells.get(0)[0],touchedCells.get(0)[1]);

                // Si il y a la mer à droite de la case touchée
                if (round[0]==-1){
                    row = touchedCells.get(0)[0];
                    col = touchedCells.get(0)[1]+1;
                    coordonates[0]=row;
                    coordonates[1]=col;
                    break;
                }
                // Sinon si il y a la mer en dessous de la case touchée
                else if (round[1] == -1){
                    row = touchedCells.get(0)[0]+1;
                    col = touchedCells.get(0)[1];
                    coordonates[0]=row;
                    coordonates[1]=col;
                    break;
                }
                // Sinon si il y a la mer à gauche de la case touchée
                else if (round[2] == -1){
                    row = touchedCells.get(0)[0];
                    col = touchedCells.get(0)[1]-1;
                    coordonates[0]=row;
                    coordonates[1]=col;
                    break;
                }
                // Sinon il y a la mer au dessus de la case touchée
                else if (round[3] == -1){
                    row = touchedCells.get(0)[0]-1;
                    col = touchedCells.get(0)[1];
                    coordonates[0]=row;
                    coordonates[1]=col;
                    break;
                }

                // On a touché à droite de la première case touchée
            case 1 :
                // Si on peut aller à droite on y va
                if (touchedCells.get(touchedCells.size()-1)[1]+1 < currentPlayer.getEnemyGrid().length
                        && currentPlayer.getEnemyGrid()[touchedCells.get(touchedCells.size()-1)[0]][touchedCells.get(touchedCells.size()-1)[1]+1] == -1) {
                    row = touchedCells.get(touchedCells.size() - 1)[0];
                    col = touchedCells.get(touchedCells.size() - 1)[1] + 1;
                    coordonates[0] = row;
                    coordonates[1] = col;
                    break;
                }
                // Sinon on va à gauche depuis la première case touchée
                else{
                    adaptativeSituation(currentPlayer,2);
                }

                // On veut aller à gauche depuis la première case touchée
            case 2 :
                // Si on peut aller à gauche on y va
                if (touchedCells.get(0)[1]-1 >= 0
                        && currentPlayer.getEnemyGrid()[touchedCells.get(0)[0]][touchedCells.get(0)[1]-1] == -1){
                    row = touchedCells.get(0)[0];
                    col = touchedCells.get(0)[1]-1;
                    coordonates[0] = row;
                    coordonates[1] = col;
                    break;
                }
                // Si on ne peut pas aller à gauche on va en vertical
                else{
                    adaptativeSituation(currentPlayer,4);
                }

                // On a déjà touché à gauche de la première case touchée
            case 3 :
                // Si on peut aller à gauche on y va
                if (touchedCells.get(touchedCells.size()-1)[1]-1 >= 0
                        && currentPlayer.getEnemyGrid()[touchedCells.get(touchedCells.size()-1)[0]][touchedCells.get(touchedCells.size()-1)[1]-1] == -1){
                    row = touchedCells.get(touchedCells.size()-1)[0];
                    col = touchedCells.get(touchedCells.size()-1)[1]-1;
                    coordonates[0] = row;
                    coordonates[1] = col;
                    break;
                }
                // Si on ne peut pas aller à gauche on va en vertical
                else {
                    adaptativeSituation(currentPlayer,4);
                }


                // On veut tirer en dessous de la première case touchée
            case 4 :
                // Si on peut aller en dessous on y va
                if (touchedCells.get(0)[0]+1 < currentPlayer.getEnemyGrid().length
                        && currentPlayer.getEnemyGrid()[touchedCells.get(0)[0]+1][touchedCells.get(0)[1]] == -1){
                    row = touchedCells.get(0)[0]+1;
                    col = touchedCells.get(0)[1];
                    coordonates[0] = row;
                    coordonates[1] = col;
                    break;
                }
                // Sinon on va au dessus
                else {
                    adaptativeSituation(currentPlayer,6);
                }

                // On a déjà touché en dessous de la première case touchée
            case 5 :
                // Si on peut aller en dessous on y va
                if (touchedCells.get(touchedCells.size()-1)[0]+1 < currentPlayer.getEnemyGrid().length
                        && currentPlayer.getEnemyGrid()[touchedCells.get(touchedCells.size()-1)[0]+1][touchedCells.get(touchedCells.size()-1)[1]] == -1){
                    row = touchedCells.get(touchedCells.size()-1)[0]+1;
                    col = touchedCells.get(touchedCells.size()-1)[1];
                    coordonates[0] = row;
                    coordonates[1] = col;
                    break;
                }
                // Sinon on va au dessus
                else {
                    adaptativeSituation(currentPlayer,6);
                }

                // On veut tirer au dessus de la première case touchée
            case 6 :
                // Si on peut aller au dessus de la première case touchée on y va
                if (touchedCells.get(0)[0]-1 >= 0
                        && currentPlayer.getEnemyGrid()[touchedCells.get(0)[0]-1][touchedCells.get(0)[1]] == -1) {
                    row = touchedCells.get(0)[0] - 1;
                    col = touchedCells.get(0)[1];
                    coordonates[0] = row;
                    coordonates[1] = col;
                    break;
                }
                // Sinon on va à gauche parce qu'on est déjà allés à droite
                else {
                    adaptativeSituation(currentPlayer,2);
                }

                // On a déjà touché au dessus de la première case touchée
            case 7 :
                // Si la dernière case touchée est sur le bateau, on continue vers le haut si on ne dépasse pas de la grille
                if (touchedCells.get(touchedCells.size()-1)[0]-1 >= 0
                        && currentPlayer.getEnemyGrid()[touchedCells.get(touchedCells.size()-1)[0]-1][touchedCells.get(touchedCells.size()-1)[1]] == -1) {
                    row = touchedCells.get(touchedCells.size() - 1)[0] - 1;
                    col = touchedCells.get(touchedCells.size() - 1)[1];
                    coordonates[0] = row;
                    coordonates[1] = col;
                    break;
                }
                // Sinon on va à gauche parce qu'on est déjà allés à droite
                else {
                    adaptativeSituation(currentPlayer,2);
                }

        }

        return coordonates;
    }

    public int[] turnAround(Player currentPlayer, int row, int col){
        int[] round = new int[4];

        if (col+1 < currentPlayer.getEnemyGrid().length)
            round[0] = currentPlayer.getEnemyGrid()[row][col+1];
        else
            round[0] = 10;

        if (row+1 < currentPlayer.getEnemyGrid().length)
            round[1]= currentPlayer.getEnemyGrid()[row+1][col];
        else
            round[1] = 10;

        if (col-1 >= 0)
            round[2]= currentPlayer.getEnemyGrid()[row][col-1];
        else
            round[2] = 10;

        if (row-1 >= 0)
            round[3]= currentPlayer.getEnemyGrid()[row-1][col];
        else
            round[3] = 10;

        return round;
    }


}