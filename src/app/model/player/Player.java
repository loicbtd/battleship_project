package app.model.player;

import app.model.ship.Ship;

public abstract class Player {

    protected int[][] playerGrid;
    protected int[][] enemyGrid;
    protected Ship[] arrShip;
    protected int[] arrBoatState;

    public int[][] getPlayerGrid() {
        return playerGrid;
    }

    public void setPlayerGrid(int[][] playerGrid) {
        this.playerGrid = playerGrid;
    }

    public int[][] getEnemyGrid() {
        return enemyGrid;
    }

    public void setEnemyGrid(int[][] enemyGrid) {
        this.enemyGrid = enemyGrid;
    }

    public Ship[] getArrShip() {
        return arrShip;
    }

    public void setArrShip(Ship[] arrShip) {
        this.arrShip = arrShip;
    }

    public int[] getArrBoatState() {
        return arrBoatState;
    }

    public void setArrBoatState(int[] arrBoatState) {
        this.arrBoatState = arrBoatState;
    }

    public void init(int playerGridRow, int playerGridCol, int enemyGridRow, int enemyGridCol, Ship[] arrShip) {
        playerGrid = new int[playerGridRow][playerGridCol];
        for (int row = 0; row < playerGrid.length; row++) {
            for (int col = 0; col < playerGrid[row].length; col++) {
                playerGrid[row][col] = -1;
            }
        }
        enemyGrid = new int[enemyGridRow][enemyGridCol];
        for (int row = 0; row < enemyGrid.length; row++) {
            for (int col = 0; col < enemyGrid[row].length; col++) {
                enemyGrid[row][col] = -1;
            }
        }
        this.arrShip = arrShip;
        arrBoatState = new int[arrShip.length];
    }

    public int placeShip(int shipIndex, int row, int col, boolean horizontal) {

        if (row < 0) {
            arrBoatState[shipIndex] = 0;
//            System.out.println("ERROR: placement - top overrun");
            return -1;
        }
        if (col < 0) {
            arrBoatState[shipIndex] = 0;
//            System.out.println("ERROR: placement - left overrun");
            return -4;
        }

        if (horizontal) {
            if (row > playerGrid.length) {
                arrBoatState[shipIndex] = 0;
//                System.out.println("ERROR: placement - bottom overrun");
                return -3;
            } else if (col + arrShip[shipIndex].getShipEnum().getSize() > playerGrid[row].length) {
                arrBoatState[shipIndex] = 0;
//                System.out.println("ERROR: placement - right overrun");
                return -2;
            }
        } else {
            if (row + arrShip[shipIndex].getShipEnum().getSize() > playerGrid.length) {
                arrBoatState[shipIndex] = 0;
//                System.out.println("ERROR: placement - bottom overrun");
                return -3;
            } else if (col > playerGrid[row].length) {
                arrBoatState[shipIndex] = 0;
//                System.out.println("ERROR: placement - right overrun");
                return -2;
            }
        }

        for (int cell = 0; cell < arrShip[shipIndex].getShipEnum().getSize(); cell++) {
            if (horizontal) {
                if (playerGrid[row][col + cell] != -1) {
                    arrBoatState[shipIndex] = 0;
//                    System.out.println("ERROR: superimposing ship");
                    return -5;
                }
            } else {
                if (playerGrid[row + cell][col] != -1) {
                    arrBoatState[shipIndex] = 0;
//                    System.out.println("ERROR: superimposing ship");
                    return -5;
                }
            }
        }

        for (int cell = 0; cell < arrShip[shipIndex].getShipEnum().getSize(); cell++) {
            if (horizontal) {
                playerGrid[row][col+cell] = shipIndex;
            }
            else {
                playerGrid[row+cell][col] = shipIndex;
            }
        }
        arrBoatState[shipIndex] = arrShip[shipIndex].getShipEnum().getSize();
        arrShip[shipIndex].setRow(row);
        arrShip[shipIndex].setCol(col);
        arrShip[shipIndex].setHorizontal(horizontal);
//        System.out.println("INFO: boat placed");
        return 0;
    }

    public abstract void placeShip();

    public void removeShip(int shipIndex) {
        int row = arrShip[shipIndex].getRow();
        int col = arrShip[shipIndex].getCol();
        boolean horizontal = arrShip[shipIndex].isHorizontal();
        int size = arrShip[shipIndex].getShipEnum().getSize();

        for (int cell = 0; cell < size; cell++) {
            if (horizontal) {
                playerGrid[row][col+cell] = -1;
            }
            else {
                playerGrid[row+cell][col] = -1;
            }
        }
        arrBoatState[shipIndex] = 0;
    }

    public int shot(int row, int col, Player enemyPlayer) {

        int cellValue = enemyPlayer.takeShot(row, col);
        if (cellValue >= 0) enemyGrid[row][col] = cellValue * (-1) - 10;  // struck !
        if (cellValue == -1 ) enemyGrid[row][col] = -2; // missed
        return cellValue;
    }

    public int takeShot(int row, int col) {

        if (row < 0) return -3;
        if (col < 0) return -6;
        if (row > playerGrid.length) return -5;
        if (col > playerGrid[0].length) return -4;

        int cellValue = playerGrid[row][col];

        // cell already shot !
        if (cellValue == -2 || cellValue <= -10) {
            return -2;
        }

        //struck !
        if (cellValue >= 0) {
            arrBoatState[cellValue]--;
            playerGrid[row][col] = cellValue * (-1) - 10;
        }

        // missed !
        if (cellValue == -1 ) {
            playerGrid[row][col] = -2;
        }
        return cellValue;
    }

    public abstract void play(Player enemyPlayer);

    public boolean hasLost() {
        for (int life : arrBoatState) {
            if (life > 0) return false;
        }
        return true;
    }


    //DEV
    public void displayGrid() {
        for (int row = 0; row < this.playerGrid.length; row++) {
            for (int col = 0; col < this.playerGrid[row].length; col++) {
                if (this.playerGrid[row][col]<0)
                    System.out.print("|"+this.playerGrid[row][col]);
                else
                    System.out.print("| "+this.playerGrid[row][col]);
            }
            System.out.println("|");
        }
    }
}
